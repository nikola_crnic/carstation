﻿using System;
using System.Collections.Generic;
using CarStation.Domain.Models;

namespace CarStation.Domain.Services
{
    public interface ICountryService
    {
        IList<CountryDomainModel> GetAll();
        CountryDomainModel GetById(Guid id);
        CountryDomainModel GetByCode(string code);
    }
}