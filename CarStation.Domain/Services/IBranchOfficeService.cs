using System;
using System.Collections.Generic;
using CarStation.Domain.Models;

namespace CarStation.Domain.Services
{
    public interface IBranchOfficeService
    {
        IList<BranchOfficeDomainModel> GetAll();
        BranchOfficeDomainModel GetById(Guid id);
        BranchOfficeDomainModel GetByName(string name);
        void Save(BranchOfficeDomainModel branchOfficeCreate);
        void Update(BranchOfficeDomainModel branchOfficeEdit);
        void Delete(Guid id);
        IList<BranchOfficeDomainModel> GetByCity(Guid locationId);
        IList<BranchOfficeRents> GetNumberOfRentsByBranchOffice();
    }
}