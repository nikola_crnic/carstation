﻿using System;
using System.Collections.Generic;
using CarStation.Domain.Models;

namespace CarStation.Domain.Services
{
    public interface IVehicleService
    {
        IList<VehicleDomainModel> GetAll();
        VehicleDomainModel GetById(Guid id);
        VehicleDomainModel GetByName(string registration);
        void Save(VehicleDomainModel vehicleCreate);
        void Update(VehicleDomainModel vehicleUpdate);
        void Delete(Guid id);
        IList<RentedVehicle> GetTopVehicles(int numberOfVehicles);
        IList<VehicleDomainModel> GetByBranchOffice(Guid branchOfficeId);
    }
}