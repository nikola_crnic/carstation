﻿using System;
using System.Collections.Generic;
using CarStation.Database.Enums;
using CarStation.Domain.Models;
using VehicleStatus = CarStation.Domain.Models.Enums.VehicleStatus;
using VehicleType = CarStation.Domain.Models.Enums.VehicleType;

namespace CarStation.Domain.Services
{
    public interface IVehicleModelService
    {
        IList<VehicleModelDomainModel> GetAll();
        VehicleModelDomainModel GetById(Guid id);
        VehicleModelDomainModel GetByName(string name);
        VehicleModelDomainModel GetByStatus(int status);
        VehicleModelDomainModel GetByType(int type);
        void Save(VehicleModelDomainModel vehicleModelCreate);
        void Update(VehicleModelDomainModel vehicleModelUpdate);
        void Delete(Guid id);
    }
}