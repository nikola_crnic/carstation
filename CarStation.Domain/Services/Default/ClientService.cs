﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CarStation.Database.Entities;
using CarStation.Database.Repositories;
using CarStation.Domain.Models;

namespace CarStation.Domain.Services.Default
{
    /// <summary>
    /// Client service
    /// </summary>
    public class ClientService : IClientService
    {
        private readonly IClientRepository _clientRepository;
        private IMapper _entityMapper;
        private IMapper _domainMapper;

        /// <summary>
        /// Client service constructor
        /// </summary>
        /// <param name="clientRepository">Client database repository</param>
        public ClientService(IClientRepository clientRepository)
        {
            _clientRepository = clientRepository;
            Mappings();
        }

        /// <summary>
        /// Gets all clients
        /// </summary>
        /// <returns>List of clients</returns>
        public IList<ClientDomainModel> GetAll()
        {
            var clients = _clientRepository.GetAll().OrderBy(b => b.Name).ToList();
            return _domainMapper.Map<IList<ClientDomainModel>>(clients);
        }

        /// <summary>
        /// Gets client by id.
        /// </summary>
        /// <param name="id">_id</param>
        /// <returns>Client object</returns>
        public ClientDomainModel GetById(Guid id)
        {
            var client = _clientRepository.GetById(id);
            return _domainMapper.Map<ClientDomainModel>(client);
        }

        /// <summary>
        /// Gets client by email.
        /// </summary>
        /// <param name="email">Email</param>
        /// <returns>Client object</returns>
        public ClientDomainModel GetByEmail(string email)
        {
            var client = _clientRepository.GetAll().FirstOrDefault(x => x.Email == email);
            return _domainMapper.Map<ClientDomainModel>(client);
        }

        /// <summary>
        /// Saves client
        /// </summary>
        /// <param name="clientEdit">Client edit object</param>
        public void Save(ClientDomainModel clientEdit)
        {
            var client = _entityMapper.Map<Client>(clientEdit);
            _clientRepository.Save(client);
        }

        /// <summary>
        /// Updates client
        /// </summary>
        /// <param name="clientEdit">CLient edit object</param>
        public void Update(ClientDomainModel clientEdit)
        {
            var client = _entityMapper.Map<Client>(clientEdit);
            _clientRepository.Update(client);
        }

        /// <summary>
        /// Deletes branch office
        /// </summary>
        /// <param name="id">_id</param>
        public void Delete(Guid id)
        {
            _clientRepository.Delete(id);
        }

        /// <summary>
        /// AutoMapper settings
        /// </summary>
        public void Mappings()
        {
            var entityConfig = new MapperConfiguration(cfg => cfg.CreateMap<ClientDomainModel, Client>());
            var domainConfig = new MapperConfiguration(cfg => cfg.CreateMap<Client, ClientDomainModel>());

            _entityMapper = entityConfig.CreateMapper();
            _domainMapper = domainConfig.CreateMapper();
        }
    }
}