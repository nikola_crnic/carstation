﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CarStation.Database.Entities;
using CarStation.Database.Repositories;
using CarStation.Domain.Models;
using CarStation.Domain.Models.Enums;

namespace CarStation.Domain.Services.Default
{
    public class RentService : IRentService
    {
        private readonly IRentRepository _rentRepository;
        private readonly IVehicleRepository _vehicleRepository;
        private IMapper _entityMapper;
        private IMapper _domainMapper;

        /// <summary>
        /// Rent service constructor
        /// </summary>
        /// <param name="rentRepository">Rent database repository</param>
        /// <param name="vehicleRepository">Client database repository</param>
        public RentService(IRentRepository rentRepository, IVehicleRepository vehicleRepository)
        {
            _rentRepository = rentRepository;
            _vehicleRepository = vehicleRepository;
            Mappings();
        }

        /// <summary>
        /// Gets all rents
        /// </summary>
        /// <returns>List of rents</returns>
        public IList<RentDomainModel> GetAll()
        {
            var rents = _rentRepository.GetAll().OrderBy(b => b.RentNumber).ToList();
            return _domainMapper.Map<IList<RentDomainModel>>(rents);
        }

        /// <summary>
        /// Gets rent by id.
        /// </summary>
        /// <param name="id">_id</param>
        /// <returns>Rent object</returns>
        public RentDomainModel GetById(Guid id)
        {
            var rent = _rentRepository.GetById(id);
            return _domainMapper.Map<RentDomainModel>(rent);
        }

        public IList<RentDomainModel> GetRentsByClient(Guid clientId)
        {
            var rents = _rentRepository.GetAll().Where(x => x.ClientId == clientId).ToList();
            return _domainMapper.Map<IList<RentDomainModel>>(rents);
        }

        /// <summary>
        /// Saves rent
        /// </summary>
        /// <param name="rentCreate">Rent domain object</param>
        public void Save(RentDomainModel rentCreate)
        {
            var rent = _entityMapper.Map<Rent>(rentCreate);
            _rentRepository.Save(rent);
        }

        /// <summary>
        /// Updates rent
        /// </summary>
        /// <param name="rentUpdate">Rent domain object</param>
        public void Update(RentDomainModel rentUpdate)
        {
            var rent = _entityMapper.Map<Rent>(rentUpdate);
            _rentRepository.Update(rent);
        }

        /// <summary>
        /// Deletes rent by id
        /// </summary>
        /// <param name="id">_id</param>
        public void Delete(Guid id)
        {
            _rentRepository.Delete(id);
        }

        public IList<int> GetByMonth()
        {
            var rents = new List<int>();
            var dbRents = _rentRepository.GetAll();
            for (int i = 1; i < 13; i++)
            {
                int counter = 0;
                foreach (var dbRent in dbRents)
                {
                    if (dbRent.RentedFrom.Month == i && dbRent.RentedFrom.Year == DateTime.Now.Year)
                        counter++;
                }
                rents.Add(counter);
            }

            return rents;
        }

        public IList<RentDomainModel> GetInProcess()
        {
            var rents = _rentRepository.GetAll().Where(x => x.Status == Database.Enums.RentStatus.NotPaid || x.Status == Database.Enums.RentStatus.Overdue);
            return _domainMapper.Map<IList<RentDomainModel>>(rents);
        }

        public decimal CalculateTotalNetAmount(Guid id)
        {
            var rent = _rentRepository.GetById(id);
            if (!rent.VehicleId.HasValue)
                return 0;
            var days = Math.Round((rent.RentedTo - rent.RentedFrom).TotalDays, 2);
            var vehicle = _vehicleRepository.GetById(rent.VehicleId.Value);
            return vehicle.PricePerDay * Convert.ToDecimal(days);
        }

        public decimal CalculateTotalGrossAmount(Guid id)
        {
            var rent = _rentRepository.GetById(id);
            var netAmount = CalculateTotalNetAmount(id);
            return netAmount*rent.Tax / 100 + netAmount;
        }

        /// <summary>
        /// AutoMapper settings
        /// </summary>
        public void Mappings()
        {
            var entityConfig = new MapperConfiguration(cfg => cfg.CreateMap<RentDomainModel, Rent>());
            var domainConfig = new MapperConfiguration(cfg => cfg.CreateMap<Rent, RentDomainModel>());

            _entityMapper = entityConfig.CreateMapper();
            _domainMapper = domainConfig.CreateMapper();
        }
    }
}