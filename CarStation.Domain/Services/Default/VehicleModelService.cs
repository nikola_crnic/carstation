﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CarStation.Database.Entities;
using CarStation.Database.Enums;
using CarStation.Database.Repositories;
using CarStation.Domain.Models;
using VehicleStatus = CarStation.Domain.Models.Enums.VehicleStatus;
using VehicleType = CarStation.Domain.Models.Enums.VehicleType;

namespace CarStation.Domain.Services.Default
{
    public class VehicleModelService : IVehicleModelService
    {
        private readonly IVehicleModelRepository _vehicleModelRepository;
        private IMapper _entityMapper;
        private IMapper _domainMapper;

        /// <summary>
        /// Vehicle model service constructor
        /// </summary>
        /// <param name="vehicleModelRepository">Vehicle model database repository</param>
        public VehicleModelService(IVehicleModelRepository vehicleModelRepository)
        {
            _vehicleModelRepository = vehicleModelRepository;
            Mappings();
        }

        /// <summary>
        /// Gets all vehicle models
        /// </summary>
        /// <returns>List of vehicle models</returns>
        public IList<VehicleModelDomainModel> GetAll()
        {
            var vehicleModels = _vehicleModelRepository.GetAll().OrderBy(b => b.Name).ToList();
            return _domainMapper.Map<IList<VehicleModelDomainModel>>(vehicleModels);
        }

        /// <summary>
        /// Gets vehicle by id.
        /// </summary>
        /// <param name="id">_id</param>
        /// <returns>Vehicle object</returns>
        public VehicleModelDomainModel GetById(Guid id)
        {
            var vehicleModel = _vehicleModelRepository.GetById(id);
            return _domainMapper.Map<VehicleModelDomainModel>(vehicleModel);
        }

        /// <summary>
        /// Vehicle model by name.
        /// </summary>
        /// <param name="name">Name</param>
        /// <returns>Vehicle model object</returns>
        public VehicleModelDomainModel GetByName(string name)
        {
            var vehicleModel = _vehicleModelRepository.GetAll().FirstOrDefault(x => x.Name == name);
            return _domainMapper.Map<VehicleModelDomainModel>(vehicleModel);
        }

        /// <summary>
        /// Vehicle model by status.
        /// </summary>
        /// <param name="status">Vehicle status</param>
        /// <returns>Vehicle model object</returns>
        public VehicleModelDomainModel GetByStatus(int status)
        {
            var statusEnum = (Database.Enums.VehicleStatus)status;
            var vehicleModel = _vehicleModelRepository.GetAll().FirstOrDefault(x => x.Status == statusEnum);
            return _domainMapper.Map<VehicleModelDomainModel>(vehicleModel);
        }

        /// <summary>
        /// Vehicle model by type.
        /// </summary>
        /// <param name="type">Vehicle type</param>
        /// <returns>Vehicle model object</returns>
        public VehicleModelDomainModel GetByType(int type)
        {
            var typeEnum = (Database.Enums.VehicleType)type;
            var vehicleModel = _vehicleModelRepository.GetAll().FirstOrDefault(x => x.Type == typeEnum);
            return _domainMapper.Map<VehicleModelDomainModel>(vehicleModel);
        }

        /// <summary>
        /// Saves vehicle model
        /// </summary>
        /// <param name="vehicleModelCreate">Vehicle model domain object</param>
        public void Save(VehicleModelDomainModel vehicleModelCreate)
        {
            var vehicleModel = _entityMapper.Map<VehicleModel>(vehicleModelCreate);
            _vehicleModelRepository.Save(vehicleModel);
        }

        /// <summary>
        /// Updates vehicle model
        /// </summary>
        /// <param name="vehicleModelUpdate">Vehicle model domain object</param>
        public void Update(VehicleModelDomainModel vehicleModelUpdate)
        {
            var vehicleModel = _entityMapper.Map<VehicleModel>(vehicleModelUpdate);
            _vehicleModelRepository.Update(vehicleModel);
        }

        /// <summary>
        /// Deletes vehicle model by id
        /// </summary>
        /// <param name="id">_id</param>
        public void Delete(Guid id)
        {
            _vehicleModelRepository.Delete(id);
        }

        /// <summary>
        /// AutoMapper settings
        /// </summary>
        public void Mappings()
        {
            var entityConfig = new MapperConfiguration(cfg => cfg.CreateMap<VehicleModelDomainModel, VehicleModel>());
            var domainConfig = new MapperConfiguration(cfg => cfg.CreateMap<VehicleModel, VehicleModelDomainModel>());

            _entityMapper = entityConfig.CreateMapper();
            _domainMapper = domainConfig.CreateMapper();
        }
    }
}