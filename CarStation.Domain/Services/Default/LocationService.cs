﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CarStation.Database.Entities;
using CarStation.Database.Repositories;
using CarStation.Domain.Models;

namespace CarStation.Domain.Services.Default
{
    /// <summary>
    /// Location service
    /// </summary>
    public class LocationService : ILocationService
    {
        private readonly ILocationRepository _locationRepository;
        private readonly ICountryRepository _countryRepository;
        private IMapper _entityMapper;
        private IMapper _domainMapper;
        private readonly ICurrencyRepository _currencyRepository;

        /// <summary>
        /// Location service constructor
        /// </summary>
        /// <param name="locationRepository">Location database repository</param>
        /// <param name="countryRepository">Country database repository</param>
        public LocationService(ILocationRepository locationRepository, ICountryRepository countryRepository, ICurrencyRepository currencyRepository)
        {
            _locationRepository = locationRepository;
            _countryRepository = countryRepository;
            _currencyRepository = currencyRepository;
            InitCountries();
            InitCurrencies();
            Mappings();
        }

        /// <summary>
        /// Gets all locations
        /// </summary>
        /// <returns>List of locations</returns>
        public IList<LocationDomainModel> GetAll()
        {
            var locations = _locationRepository.GetAll().OrderBy(b => b.Name).ToList();
            return _domainMapper.Map<IList<LocationDomainModel>>(locations);
        }

        /// <summary>
        /// Gets location by id.
        /// </summary>
        /// <param name="id">_id</param>
        /// <returns>Location object</returns>
        public LocationDomainModel GetById(Guid id)
        {
            var location = _locationRepository.GetById(id);
            return _domainMapper.Map<LocationDomainModel>(location);
        }

        /// <summary>
        /// Location by name.
        /// </summary>
        /// <param name="name">Name</param>
        /// <returns>Location object</returns>
        public LocationDomainModel GetByName(string name)
        {
            var location = _locationRepository.GetAll().FirstOrDefault(x => x.Name == name);
            return _domainMapper.Map<LocationDomainModel>(location);
        }

        /// <summary>
        /// Saves location
        /// </summary>
        /// <param name="locationCreate">Location domain object</param>
        public void Save(LocationDomainModel locationCreate)
        {
            var location = _entityMapper.Map<Location>(locationCreate);
            _locationRepository.Save(location);
        }

        /// <summary>
        /// Updates location
        /// </summary>
        /// <param name="locationUpdate">Location domain object</param>
        public void Update(LocationDomainModel locationUpdate)
        {
            var location = _entityMapper.Map<Location>(locationUpdate);
            _locationRepository.Update(location);
        }

        /// <summary>
        /// Deletes location by id
        /// </summary>
        /// <param name="id">_id</param>
        public void Delete(Guid id)
        {
            _locationRepository.Delete(id);
        }

        /// <summary>
        /// AutoMapper settings
        /// </summary>
        public void Mappings()
        {
            var entityConfig = new MapperConfiguration(cfg => cfg.CreateMap<LocationDomainModel, Location>());
            var domainConfig = new MapperConfiguration(cfg => cfg.CreateMap<Location, LocationDomainModel>());

            _entityMapper = entityConfig.CreateMapper();
            _domainMapper = domainConfig.CreateMapper();
        }

        private void InitCountries()
        {
            var added = _countryRepository.GetAll().Any();
            if (!added)
                _countryRepository.Save();
        }

        private void InitCurrencies()
        {
            var added = _currencyRepository.GetAll().Any();
            if (!added)
                _currencyRepository.Save();
        }
    }
}