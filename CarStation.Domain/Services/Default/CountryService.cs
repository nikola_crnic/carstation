﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CarStation.Database.Entities;
using CarStation.Database.Repositories;
using CarStation.Domain.Models;

namespace CarStation.Domain.Services.Default
{
    public class CountryService : ICountryService
    {
        private readonly ICountryRepository _countryRepository;
        private IMapper _domainMapper;

        public CountryService(ICountryRepository countryRepository)
        {
            _countryRepository = countryRepository;
            Mappings();
        }

        /// <summary>
        /// Gets all countries
        /// </summary>
        /// <returns>List of countries</returns>
        public IList<CountryDomainModel> GetAll()
        {
            var countries = _countryRepository.GetAll().OrderBy(b => b.name).ToList();
            return _domainMapper.Map<IList<CountryDomainModel>>(countries);
        }

        /// <summary>
        /// Gets client by id.
        /// </summary>
        /// <param name="id">_id</param>
        /// <returns>Client object</returns>
        public CountryDomainModel GetById(Guid id)
        {
            var country = _countryRepository.GetById(id);
            return _domainMapper.Map<CountryDomainModel>(country);
        }

        /// <summary>
        /// Gets country by code.
        /// </summary>
        /// <param name="code">Country code</param>
        /// <returns>Country object</returns>
        public CountryDomainModel GetByCode(string code)
        {
            var country = _countryRepository.GetAll().FirstOrDefault(x => x.code == code);
            return _domainMapper.Map<CountryDomainModel>(country);
        }

        /// <summary>
        /// AutoMapper settings
        /// </summary>
        public void Mappings()
        {
            var domainConfig = new MapperConfiguration(cfg => cfg.CreateMap<Country, CountryDomainModel>());
            _domainMapper = domainConfig.CreateMapper();
        }
    }
}