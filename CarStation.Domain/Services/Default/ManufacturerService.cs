﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CarStation.Database.Entities;
using CarStation.Database.Repositories;
using CarStation.Domain.Models;

namespace CarStation.Domain.Services.Default
{
    public class ManufacturerService : IManufacturerService
    {
        private readonly IManufacturerRepository _manufacturerRepository;
        private IMapper _entityMapper;
        private IMapper _domainMapper;

        /// <summary>
        /// Branch office service constructor
        /// </summary>
        /// <param name="manufacturerRepository">Manufacturer database repository</param>
        public ManufacturerService(IManufacturerRepository manufacturerRepository)
        {
            _manufacturerRepository = manufacturerRepository;
            Mappings();
        }

        /// <summary>
        /// Gets all branch offices
        /// </summary>
        /// <returns>List of manufacturers</returns>
        public IList<ManufacturerDomainModel> GetAll()
        {
            var branchOffices = _manufacturerRepository.GetAll().OrderBy(b => b.Name).ToList();
            return _domainMapper.Map<IList<ManufacturerDomainModel>>(branchOffices);
        }

        /// <summary>
        /// Gets branch office by id.
        /// </summary>
        /// <param name="id">_id</param>
        /// <returns>Branch office object</returns>
        public ManufacturerDomainModel GetById(Guid id)
        {
            var branchOffice = _manufacturerRepository.GetById(id);
            return _domainMapper.Map<ManufacturerDomainModel>(branchOffice);
        }

        /// <summary>
        /// Gets branch office by name.
        /// </summary>
        /// <param name="name">Name</param>
        /// <returns>Branch office object</returns>
        public ManufacturerDomainModel GetByName(string name)
        {
            var branchOffice = _manufacturerRepository.GetAll().FirstOrDefault(x => x.Name == name);
            return _domainMapper.Map<ManufacturerDomainModel>(branchOffice);
        }

        /// <summary>
        /// Saves branch office
        /// </summary>
        /// <param name="branchOfficeCreate">Branch office domain</param>
        public void Save(ManufacturerDomainModel branchOfficeCreate)
        {
            var branchOffice = _entityMapper.Map<Manufacturer>(branchOfficeCreate);
            _manufacturerRepository.Save(branchOffice);
        }

        /// <summary>
        /// Updates branch office
        /// </summary>
        /// <param name="branchOfficeEdit"></param>
        public void Update(ManufacturerDomainModel branchOfficeEdit)
        {
            var branchOffice = _entityMapper.Map<Manufacturer>(branchOfficeEdit);
            _manufacturerRepository.Update(branchOffice);
        }

        /// <summary>
        /// Deletes branch office
        /// </summary>
        /// <param name="id">_id</param>
        public void Delete(Guid id)
        {
            _manufacturerRepository.Delete(id);
        }

        /// <summary>
        /// AutoMapper settings
        /// </summary>
        private void Mappings()
        {
            var entityConfig = new MapperConfiguration(cfg => cfg.CreateMap<ManufacturerDomainModel, Manufacturer>());
            var domainConfig = new MapperConfiguration(cfg => cfg.CreateMap<Manufacturer, ManufacturerDomainModel>());

            _entityMapper = entityConfig.CreateMapper();
            _domainMapper = domainConfig.CreateMapper();
        }
    }
}