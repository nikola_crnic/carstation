﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CarStation.Database.Entities;
using CarStation.Database.Repositories;
using CarStation.Domain.Models;

namespace CarStation.Domain.Services.Default
{
    /// <summary>
    /// Vehicle service
    /// </summary>
    public class VehicleService : IVehicleService
    {
        private readonly IVehicleRepository _vehicleRepository;
        private IMapper _entityMapper;
        private IMapper _domainMapper;
        private IRentRepository _rentRepository;
        private IManufacturerRepository _manufacturerRepository;
        private IVehicleModelRepository _vehicleModelRepository;

        /// <summary>
        /// Vehicle service constructor
        /// </summary>
        /// <param name="vehicleRepository">Vehicle database repository</param>
        public VehicleService(IVehicleRepository vehicleRepository, IRentRepository rentRepository, IManufacturerRepository manufacturerRepository, IVehicleModelRepository vehicleModelRepository)
        {
            _vehicleRepository = vehicleRepository;
            _rentRepository = rentRepository;
            _manufacturerRepository = manufacturerRepository;
            _vehicleModelRepository = vehicleModelRepository;
            Mappings();
        }

        public VehicleService(IRentRepository rentRepository, IManufacturerRepository manufacturerRepository, IVehicleModelRepository vehicleModelRepository)
        {
            _rentRepository = rentRepository;
            _manufacturerRepository = manufacturerRepository;
            _vehicleModelRepository = vehicleModelRepository;
        }

        /// <summary>
        /// Gets all vehicles
        /// </summary>
        /// <returns>List of vehicles</returns>
        public IList<VehicleDomainModel> GetAll()
        {
            var vehicles = _vehicleRepository.GetAll().OrderBy(b => b.BranchOfficeId).ToList();
            return _domainMapper.Map<IList<VehicleDomainModel>>(vehicles);
        }

        /// <summary>
        /// Gets vehicle by id.
        /// </summary>
        /// <param name="id">_id</param>
        /// <returns>Vehicle object</returns>
        public VehicleDomainModel GetById(Guid id)
        {
            var vehicle = _vehicleRepository.GetById(id);
            return _domainMapper.Map<VehicleDomainModel>(vehicle);
        }

        /// <summary>
        /// Vehicle by name.
        /// </summary>
        /// <param name="registration">Name</param>
        /// <returns>Vehicle object</returns>
        public VehicleDomainModel GetByName(string registration)
        {
            var vehicle = _vehicleRepository.GetAll().FirstOrDefault(x => x.RegistrationNumber == registration);
            return _domainMapper.Map<VehicleDomainModel>(vehicle);
        }

        /// <summary>
        /// Vehicle by branch office.
        /// </summary>
        /// <param name="branchOfficeId"></param>
        /// <returns>Vehicle object</returns>
        public IList<VehicleDomainModel> GetByBranchOffice(Guid branchOfficeId)
        {
            var vehicles = _vehicleRepository.GetAll().Where(x => x.BranchOfficeId == branchOfficeId);
            return _domainMapper.Map<IList<VehicleDomainModel>>(vehicles);
        }

        /// <summary>
        /// Saves vehicle
        /// </summary>
        /// <param name="vehicleCreate">Vehicle domain object</param>
        public void Save(VehicleDomainModel vehicleCreate)
        {
            var location = _entityMapper.Map<Vehicle>(vehicleCreate);
            _vehicleRepository.Save(location);
        }

        /// <summary>
        /// Updates vehicle
        /// </summary>
        /// <param name="vehicleUpdate">Vehicle domain object</param>
        public void Update(VehicleDomainModel vehicleUpdate)
        {
            var vehicle = _entityMapper.Map<Vehicle>(vehicleUpdate);
            _vehicleRepository.Update(vehicle);
        }

        /// <summary>
        /// Deletes vehicle by id
        /// </summary>
        /// <param name="id">_id</param>
        public void Delete(Guid id)
        {
            _vehicleRepository.Delete(id);
        }

        public IList<RentedVehicle> GetTopVehicles(int numberOfVehicles)
        {
            var rentedVehicleIds =
                _rentRepository.GetAll().Where(x => x.Status != Database.Enums.RentStatus.Draft).GroupBy(x => x.VehicleId).OrderByDescending(x => x.Count())
                .Select(y => y.Key.Value).Take(numberOfVehicles);
            var rentedVehicles = new List<RentedVehicle>();

            foreach (var rentedVehicleId in rentedVehicleIds)
            {
                var vehicle = _vehicleRepository.GetById(rentedVehicleId);
                var vehicleModel = _vehicleModelRepository.GetById(vehicle.VehicleModelId);
                var manufacturer = _manufacturerRepository.GetById(vehicleModel.ManufacturerId);

                rentedVehicles.Add(new RentedVehicle
                {
                    _id = rentedVehicleId,
                    Manufacturer = manufacturer.Name,
                    Picture = vehicle.Picture,
                    RegistrationNumber = vehicle.RegistrationNumber,
                    VehicleModel = vehicleModel.Name,
                    YearOfProduction = vehicle.YearOfProduction
                });
            }
            
            return rentedVehicles;
        }

        /// <summary>
        /// AutoMapper settings
        /// </summary>
        public void Mappings()
        {
            var entityConfig = new MapperConfiguration(cfg => cfg.CreateMap<VehicleDomainModel, Vehicle>());
            var domainConfig = new MapperConfiguration(cfg => cfg.CreateMap<Vehicle, VehicleDomainModel>());

            _entityMapper = entityConfig.CreateMapper();
            _domainMapper = domainConfig.CreateMapper();
        }
    }
}