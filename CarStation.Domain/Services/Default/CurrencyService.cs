﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CarStation.Database.Entities;
using CarStation.Database.Repositories;
using CarStation.Domain.Models;

namespace CarStation.Domain.Services.Default
{
    public class CurrencyService : ICurrencyService
    {
        private readonly ICurrencyRepository _currencyRepository;
        private IMapper _domainMapper;

        public CurrencyService(ICurrencyRepository currencyRepository)
        {
            _currencyRepository = currencyRepository;
            Mappings();
        }

        /// <summary>
        /// Gets all currencies
        /// </summary>
        /// <returns>List of currencies</returns>
        public IList<CurrencyDomainModel> GetAll()
        {
            var currencies = _currencyRepository.GetAll().OrderBy(b => b.name).ToList();
            return _domainMapper.Map<IList<CurrencyDomainModel>>(currencies);
        }

        /// <summary>
        /// Gets currency by id.
        /// </summary>
        /// <param name="id">_id</param>
        /// <returns>Currency object</returns>
        public CurrencyDomainModel GetById(Guid id)
        {
            var currency = _currencyRepository.GetById(id);
            return _domainMapper.Map<CurrencyDomainModel>(currency);
        }

        /// <summary>
        /// Gets currency by code.
        /// </summary>
        /// <param name="code">Country code</param>
        /// <returns>Currency object</returns>
        public CurrencyDomainModel GetByCode(string code)
        {
            var currency = _currencyRepository.GetAll().FirstOrDefault(x => x.code == code);
            return _domainMapper.Map<CurrencyDomainModel>(currency);
        }

        /// <summary>
        /// AutoMapper settings
        /// </summary>
        public void Mappings()
        {
            var domainConfig = new MapperConfiguration(cfg => cfg.CreateMap<Currency, CurrencyDomainModel>());
            _domainMapper = domainConfig.CreateMapper();
        }
    }
}