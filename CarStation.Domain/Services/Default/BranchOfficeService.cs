﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CarStation.Database.Entities;
using CarStation.Database.Repositories;
using CarStation.Domain.Models;

namespace CarStation.Domain.Services.Default
{
    /// <summary>
    /// Branch office service
    /// </summary>
    public class BranchOfficeService : IBranchOfficeService
    {
        private readonly IBranchOfficeRepository _branchOfficeRepository;
        private readonly IRentRepository _rentRepository;
        private IMapper _entityMapper;
        private IMapper _domainMapper;

        /// <summary>
        /// Branch office service constructor
        /// </summary>
        /// <param name="branchOfficeRepository">Branch office database repository</param>
        /// <param name="rentRepository">Rent database repository.</param>
        public BranchOfficeService(IBranchOfficeRepository branchOfficeRepository, IRentRepository rentRepository)
        {
            _branchOfficeRepository = branchOfficeRepository;
            _rentRepository = rentRepository;
            Mappings();
        }

        /// <summary>
        /// Gets all branch offices
        /// </summary>
        /// <returns>List of branch offices</returns>
        public IList<BranchOfficeDomainModel> GetAll()
        {
            var branchOffices = _branchOfficeRepository.GetAll().OrderBy(b => b.Name).ToList();
            return _domainMapper.Map<IList<BranchOfficeDomainModel>>(branchOffices);
        }

        /// <summary>
        /// Gets branch office by id.
        /// </summary>
        /// <param name="id">_id</param>
        /// <returns>Branch office object</returns>
        public BranchOfficeDomainModel GetById(Guid id)
        {
            var branchOffice = _branchOfficeRepository.GetById(id);
            return _domainMapper.Map<BranchOfficeDomainModel>(branchOffice);
        }

        /// <summary>
        /// Gets branch office by name.
        /// </summary>
        /// <param name="name">Name</param>
        /// <returns>Branch office object</returns>
        public BranchOfficeDomainModel GetByName(string name)
        {
            var branchOffice = _branchOfficeRepository.GetAll().FirstOrDefault(x => x.Name == name);
            return _domainMapper.Map<BranchOfficeDomainModel>(branchOffice);
        }

        /// <summary>
        /// Gets branch office by name.
        /// </summary>
        /// <param name="locationId">Location id</param>
        /// <returns>Branch office object</returns>
        public IList<BranchOfficeDomainModel> GetByCity(Guid locationId)
        {
            var branchOffice = _branchOfficeRepository.GetAll().Where(x => x.LocationId == locationId);
            return _domainMapper.Map<IList<BranchOfficeDomainModel>>(branchOffice);
        }

        /// <summary>
        /// Gets number of rents per branch office.
        /// </summary>
        /// <returns></returns>
        public IList<BranchOfficeRents> GetNumberOfRentsByBranchOffice()
        {
            var offices = (from r in _rentRepository.GetAll()
                join b in _branchOfficeRepository.GetAll() on r.OfficeId equals b._id
                group new {r, b} by new { b._id }
                into g
                select new BranchOfficeRents
                {
                    _id = g.Key._id,
                    Street = g.Max(x => x.b.Street),
                    Name = g.Max(x => x.b.Name),
                    LocationId = g.Max(x => x.b.LocationId),
                    NumberOfRents = g.Count()
                }).ToList();

            return offices;
        }

        /// <summary>
        /// Saves branch office
        /// </summary>
        /// <param name="branchOfficeCreate">Branch office domain</param>
        public void Save(BranchOfficeDomainModel branchOfficeCreate)
        {
            var branchOffice = _entityMapper.Map<BranchOffice>(branchOfficeCreate);
            _branchOfficeRepository.Save(branchOffice);
        }

        /// <summary>
        /// Updates branch office
        /// </summary>
        /// <param name="branchOfficeEdit"></param>
        public void Update(BranchOfficeDomainModel branchOfficeEdit)
        {
            var branchOffice = _entityMapper.Map<BranchOffice>(branchOfficeEdit);
            _branchOfficeRepository.Update(branchOffice);
        }

        /// <summary>
        /// Deletes branch office
        /// </summary>
        /// <param name="id">_id</param>
        public void Delete(Guid id)
        {
            _branchOfficeRepository.Delete(id);
        }

        /// <summary>
        /// AutoMapper settings
        /// </summary>
        private void Mappings()
        {
            var entityConfig = new MapperConfiguration(cfg => cfg.CreateMap<BranchOfficeDomainModel, BranchOffice>());
            var domainConfig = new MapperConfiguration(cfg => cfg.CreateMap<BranchOffice, BranchOfficeDomainModel>());

            _entityMapper = entityConfig.CreateMapper();
            _domainMapper = domainConfig.CreateMapper();
        }
    }
}