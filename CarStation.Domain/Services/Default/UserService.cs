﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CarStation.Database.Entities;
using CarStation.Database.Repositories;
using CarStation.Domain.Models;

namespace CarStation.Domain.Services.Default
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private IMapper _entityMapper;
        private IMapper _domainMapper;

        /// <summary>
        /// User service constructor
        /// </summary>
        /// <param name="userRepository">User database repository</param>
        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
            Mappings();
        }

        /// <summary>
        /// Gets all users
        /// </summary>
        /// <returns>List of users</returns>
        public IList<UserDomainModel> GetAll()
        {
            var users = _userRepository.GetAll().OrderBy(b => b.Email).ToList();
            return _domainMapper.Map<IList<UserDomainModel>>(users);
        }

        /// <summary>
        /// Gets user by id.
        /// </summary>
        /// <param name="id">_id</param>
        /// <returns>User object</returns>
        public UserDomainModel GetById(Guid id)
        {
            var user = _userRepository.GetById(id);
            return _domainMapper.Map<UserDomainModel>(user);
        }

        /// <summary>
        /// Gets user by email.
        /// </summary>
        /// <param name="email">Email</param>
        /// <returns>User object</returns>
        public UserDomainModel GetByEmail(string email)
        {
            var client = _userRepository.GetAll().FirstOrDefault(x => x.Email == email);
            return _domainMapper.Map<UserDomainModel>(client);
        }

        /// <summary>
        /// Saves user
        /// </summary>
        /// <param name="userCreate">User edit object</param>
        public void Save(UserDomainModel userCreate)
        {
            var user = _entityMapper.Map<User>(userCreate);
            _userRepository.Save(user);
        }

        /// <summary>
        /// Deletes user
        /// </summary>
        /// <param name="id">_id</param>
        public void Delete(Guid id)
        {
            _userRepository.Delete(id);
        }

        /// <summary>
        /// AutoMapper settings
        /// </summary>
        public void Mappings()
        {
            var entityConfig = new MapperConfiguration(cfg => cfg.CreateMap<UserDomainModel, User>());
            var domainConfig = new MapperConfiguration(cfg => cfg.CreateMap<User, UserDomainModel>());

            _entityMapper = entityConfig.CreateMapper();
            _domainMapper = domainConfig.CreateMapper();
        }
    }
}