﻿using System;
using System.Collections.Generic;
using CarStation.Domain.Models;

namespace CarStation.Domain.Services
{
    public interface IUserService
    {
        IList<UserDomainModel> GetAll();
        UserDomainModel GetById(Guid id);
        UserDomainModel GetByEmail(string email);
        void Save(UserDomainModel userCreate);
        void Delete(Guid id);
    }
}