﻿using System;
using System.Collections.Generic;
using CarStation.Domain.Models;

namespace CarStation.Domain.Services
{
    public interface IClientService
    {
        IList<ClientDomainModel> GetAll();
        ClientDomainModel GetById(Guid id);
        ClientDomainModel GetByEmail(string email);
        void Save(ClientDomainModel clientEdit);
        void Update(ClientDomainModel clientEdit);
        void Delete(Guid id);
    }
}