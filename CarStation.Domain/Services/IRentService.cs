﻿using System;
using System.Collections.Generic;
using CarStation.Database.Entities;
using CarStation.Domain.Models;

namespace CarStation.Domain.Services
{
    public interface IRentService
    {
        IList<RentDomainModel> GetAll();
        RentDomainModel GetById(Guid id);
        IList<RentDomainModel> GetRentsByClient(Guid clientId);
        void Save(RentDomainModel rentCreate);
        void Update(RentDomainModel rentUpdate);
        void Delete(Guid id);
        IList<int> GetByMonth();
        IList<RentDomainModel> GetInProcess();
        decimal CalculateTotalNetAmount(Guid id);
        decimal CalculateTotalGrossAmount(Guid id);
    }
}