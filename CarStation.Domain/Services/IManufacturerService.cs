using System;
using System.Collections.Generic;
using CarStation.Database.Entities;
using CarStation.Domain.Models;

namespace CarStation.Domain.Services
{
    public interface IManufacturerService
    {
        IList<ManufacturerDomainModel> GetAll();
        ManufacturerDomainModel GetById(Guid id);
        ManufacturerDomainModel GetByName(string name);
        void Save(ManufacturerDomainModel branchOfficeCreate);
        void Update(ManufacturerDomainModel branchOfficeEdit);
        void Delete(Guid id);
    }
}