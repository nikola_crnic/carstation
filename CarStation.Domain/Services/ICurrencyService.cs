﻿using System;
using System.Collections.Generic;
using CarStation.Domain.Models;

namespace CarStation.Domain.Services
{
    public interface ICurrencyService
    {
        IList<CurrencyDomainModel> GetAll();
        CurrencyDomainModel GetById(Guid id);
        CurrencyDomainModel GetByCode(string code);
    }
}