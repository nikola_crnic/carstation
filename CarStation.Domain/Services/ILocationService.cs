﻿using System;
using System.Collections.Generic;
using CarStation.Domain.Models;

namespace CarStation.Domain.Services
{
    public interface ILocationService
    {
        IList<LocationDomainModel> GetAll();
        LocationDomainModel GetById(Guid id);
        LocationDomainModel GetByName(string name);
        void Save(LocationDomainModel branchOfficeCreate);
        void Update(LocationDomainModel branchOfficeEdit);
        void Delete(Guid id);
    }
}