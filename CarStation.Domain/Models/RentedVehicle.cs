﻿using System;

namespace CarStation.Domain.Models
{
    public class RentedVehicle
    {
        public Guid _id { get; set; }
        public string Manufacturer { get; set; } 
        public string VehicleModel { get; set; } 
        public int YearOfProduction { get; set; } 
        public string RegistrationNumber { get; set; } 
        public string Picture { get; set; } 
    }
}