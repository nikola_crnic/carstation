﻿using System;
using CarStation.Database.Enums;
using Fuel = CarStation.Domain.Models.Enums.Fuel;
using VehicleStatus = CarStation.Domain.Models.Enums.VehicleStatus;
using VehicleType = CarStation.Domain.Models.Enums.VehicleType;

namespace CarStation.Domain.Models
{
    /// <summary>
    /// Vehicle domain model
    /// </summary>
    public class VehicleModelDomainModel
    {
        /// <summary>
        /// Id
        /// </summary>
        public Guid _id { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Number of passengers
        /// </summary>
        public int Passengers { get; set; }

        /// <summary>
        /// Indicates if vehicle has automatic transmission
        /// </summary>
        public bool IsAutomatic { get; set; }

        /// <summary>
        /// Manufacturer id
        /// </summary>
        public Guid ManufacturerId { get; set; }

        /// <summary>
        /// Type of fuel
        /// </summary>
        public Fuel Fuel { get; set; } = Fuel.Diesel;

        /// <summary>
        /// Type of vehicle
        /// </summary>
        public VehicleType Type { get; set; } = VehicleType.Compact;

        /// <summary>
        /// Status
        /// </summary>
        public VehicleStatus Status { get; set; } = VehicleStatus.Available;
    }
}