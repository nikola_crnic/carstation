﻿using System;

namespace CarStation.Domain.Models
{
    /// <summary>
    /// Location domain model
    /// </summary>
    public class LocationDomainModel
    {
        /// <summary>
        /// _id
        /// </summary>
        public Guid _id { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Country id
        /// </summary>
        public Guid CountryId { get; set; } 
        public Guid CurrencyId { get; set; }
        public decimal Tax { get; set; }
    }
}