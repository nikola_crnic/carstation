﻿using System;

namespace CarStation.Domain.Models
{
    /// <summary>
    /// Branch office domain model
    /// </summary>
    public class BranchOfficeDomainModel
    {
        /// <summary>
        /// _id
        /// </summary>
        public Guid _id { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Street
        /// </summary>
        public string Street { get; set; }

        /// <summary>
        /// Location id
        /// </summary>
        public Guid LocationId { get; set; }

        /// <summary>
        /// Telephone number
        /// </summary>
        public string Telephone { get; set; }
    }
}