﻿using System;
using CarStation.Database.Enums;
using CarStation.Domain.Models.Enums;
using UserRole = CarStation.Domain.Models.Enums.UserRole;

namespace CarStation.Domain.Models
{
    /// <summary>
    /// User model
    /// </summary>
    public class UserDomainModel
    {
        public Guid _id { get; set; }
        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Password
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Random data that hashes password
        /// </summary>
        public string PasswordSalt { get; set; }

        /// <summary>
        /// User role
        /// </summary>
        public UserRole Role { get; set; } = UserRole.User;
    }
}