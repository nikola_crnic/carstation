﻿using System;

namespace CarStation.Domain.Models
{
    public class BranchOfficeRents
    {
        public Guid _id;
        public string Street { get; set; }
        public string Name { get; set; }
        public Guid LocationId { get; set; }
        public int NumberOfRents { get; set; }
    }
}