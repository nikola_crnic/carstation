﻿using System;

namespace CarStation.Domain.Models
{
    /// <summary>
    /// Country domain model
    /// </summary>
    public class CountryDomainModel
    {
        /// <summary>
        /// Id
        /// </summary>
        public Guid _id { get; set; }

        /// <summary>
        /// name
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// code
        /// </summary>
        public string code { get; set; } 
    }
}