﻿using System;
using CarStation.Database.Repositories;
using CarStation.Domain.Models.Enums;
using CarStation.Domain.Services.Default;

namespace CarStation.Domain.Models
{
    /// <summary>
    /// Rent domain model
    /// </summary>
    public class RentDomainModel
    {
        public Guid _id { get; set; }

        /// <summary>
        /// Note
        /// </summary>
        public string Note { get; set; }

        /// <summary>
        /// Rent number
        /// </summary>
        public string RentNumber { get; set; }

        /// <summary>
        /// Total net amount
        /// </summary>
        public decimal? TotalNetAmount { get; set; }

        /// <summary>
        /// Total gross amount
        /// </summary>
        public decimal? TotalGrossAmount { get; set; }

        /// <summary>
        /// Rented from date
        /// </summary>
        public DateTime RentedFrom { get; set; }

        /// <summary>
        /// Rented to date
        /// </summary>
        public DateTime RentedTo { get; set; }

        /// <summary>
        /// Client id
        /// </summary>
        public Guid? ClientId { get; set; }

        /// <summary>
        /// Office id
        /// </summary>
        public Guid OfficeId { get; set; }

        /// <summary>
        /// Vehicle id
        /// </summary>
        public Guid? VehicleId { get; set; }
        public decimal Tax { get; set; }

        /// <summary>
        /// Rent status
        /// </summary>
        public RentStatus Status { get; set; } = RentStatus.Draft;
    }
}