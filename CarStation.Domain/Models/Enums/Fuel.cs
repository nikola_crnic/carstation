﻿namespace CarStation.Domain.Models.Enums
{
    public enum Fuel
    {
         Gasoline = 1,
         Diesel = 2,
         Hybrid = 3,
         Electric = 4,
         Lpg = 5
    }
}