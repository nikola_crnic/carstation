﻿namespace CarStation.Domain.Models.Enums
{
    /// <summary>
    /// Vehicle status
    /// </summary>
    public enum VehicleStatus
    {
         Rented = 1,
         Available = 2,
         Broken = 3
    }
}