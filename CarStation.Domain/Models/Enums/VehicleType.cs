﻿namespace CarStation.Domain.Models.Enums
{
    /// <summary>
    /// Vehicle type
    /// </summary>
    public enum VehicleType
    {
         Compact = 1,
         MidSize = 2,
         Large = 3,
         Convertible = 4,
         Minivan = 5,
         Roadster = 6,
         Suv = 7,
         Van = 8,
         MicroCar = 9
    }
}