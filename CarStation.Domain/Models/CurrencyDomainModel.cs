﻿using System;

namespace CarStation.Domain.Models
{
    public class CurrencyDomainModel
    {
        /// <summary>
        /// Id
        /// </summary>
        public Guid _id { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string name_plural { get; set; }

        /// <summary>
        /// Code
        /// </summary>
        public string code { get; set; }

        /// <summary>
        /// Symbol
        /// </summary>
        public string symbol { get; set; }

        /// <summary>
        /// Native symbol
        /// </summary>
        public string symbol_native { get; set; }

        /// <summary>
        /// Decimal digits
        /// </summary>
        public decimal decimal_digits { get; set; }

        /// <summary>
        /// Decimal rounding
        /// </summary>
        public decimal rounding { get; set; }
    }
}