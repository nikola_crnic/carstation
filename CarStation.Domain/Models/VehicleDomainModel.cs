﻿using System;

namespace CarStation.Domain.Models
{
    /// <summary>
    /// Vehicle domain model
    /// </summary>
    public class VehicleDomainModel
    {
        /// <summary>
        /// Id
        /// </summary>
        public Guid _id { get; set; }

        /// <summary>
        /// Registration number
        /// </summary>
        public string RegistrationNumber { get; set; }

        /// <summary>
        /// Color in hex value
        /// </summary>
        public string Color { get; set; }

        /// <summary>
        /// Price per day
        /// </summary>
        public decimal PricePerDay { get; set; }

        /// <summary>
        /// Year of production
        /// </summary>
        public int YearOfProduction { get; set; }

        /// <summary>
        /// Picture
        /// </summary>
        public string Picture { get; set; }

        /// <summary>
        /// Branch office id
        /// </summary>
        public Guid BranchOfficeId { get; set; }

        /// <summary>
        /// Vehicle model id
        /// </summary>
        public Guid VehicleModelId { get; set; }
    }
}