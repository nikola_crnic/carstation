﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CarStation.Domain.Models
{
    public class ManufacturerDomainModel
    {
        public Guid _id { get; set; }
        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Logo
        /// </summary>
        public string Logo { get; set; }
    }
}