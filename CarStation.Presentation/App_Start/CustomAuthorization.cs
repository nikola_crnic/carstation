﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CarStation.Presentation
{

    public class CustomAuthorization : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext.HttpContext.User.Identity == null || !filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                filterContext.Result = new RedirectResult("/User/Login");
            }
        }
    }
}