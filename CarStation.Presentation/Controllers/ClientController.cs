﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CarStation.Domain.Models;
using CarStation.Domain.Services;
using CarStation.Presentation.Models;

namespace CarStation.Presentation.Controllers
{
    public class ClientController : Controller
    {
        private readonly IClientService _clientService;
        private readonly IRentService _rentService;
        private readonly ClientMapper _clientMapper;

        public ClientController(IClientService clientService, IRentService rentService, ClientMapper clientMapper)
        {
            _clientService = clientService;
            _rentService = rentService;
            _clientMapper = clientMapper;
        }

        /// <summary>
        /// Prikazuje listu klijenata.
        /// </summary>
        /// <returns></returns>
        [CustomAuthorization]
        public ActionResult Index()
        {
            var clientDomainModels = _clientService.GetAll();
            var clients = _clientMapper.EntityMapper.Map<IList<Client>>(clientDomainModels);
            return View(clients);
        }

        /// <summary>
        /// Prikazuje formu za kreiranje novog klijenta.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Create()
        {
            var model = new ClientDomainModel();
            return View(model);
        }

        /// <summary>
        /// Kreira novog klijenta.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Create(ClientDomainModel model)
        {
            if (ModelState.IsValid)
            {
                if (_clientService.GetByEmail(model.Email) != null)
                {
                    return Json("ERROR_POSTOJI");
                }
                else
                {
                    _clientService.Save(model);
                    return Json("OK");
                }
            }
            return Json("ERROR");
        }

        /// <summary>
        /// Prikazuje formu za uređivanje klijentskog naloga.
        /// </summary>
        /// <param name="Email"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Edit(string Email)
        {
            var client = _clientService.GetByEmail(Email);
            ViewBag.Zakupi = _rentService.GetRentsByClient(client._id);

            var model = new ClientDomainModel
            {
                Name = client.Name,
                Email = client.Email,
                DrivingLicence = client.DrivingLicence,
                IdentityCard = client.IdentityCard,
                Telephone = client.Telephone
            };

            return View(model);

        }

        /// <summary>
        /// Ažurira klijentski nalog.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Edit(ClientDomainModel model)
        {
            if (ModelState.IsValid)
            {
                _clientService.Update(model);
                return Json("OK");
            }
            return View();
        }

        /// <summary>
        /// Prikazuje detalje traženog klijenta.
        /// </summary>
        /// <param name="Email"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult Details(string Email)
        {
            var clientDomainModel = _clientService.GetByEmail(Email);
            var client = _clientMapper.EntityMapper.Map<Client>(clientDomainModel);

            return client != null ? Json(client, JsonRequestBehavior.AllowGet) : Json("ERROR");
        }

        /// <summary>
        /// Briše klijenta prema ID-u.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [CustomAuthorization]
        public JsonResult Delete(Guid ID)
        {
            _clientService.Delete(ID);

            return Json("OK");
        }
    }
}