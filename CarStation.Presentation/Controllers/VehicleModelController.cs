﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CarStation.Domain.Models;
using CarStation.Domain.Models.Enums;
using CarStation.Domain.Services;
using CarStation.Presentation.Models;

namespace CarStation.Presentation.Controllers
{
    public class VehicleModelController : Controller
    {
        private readonly IVehicleModelService _vehicleModelService;
        private readonly IManufacturerService _manufacturerService;
        private readonly VehicleModelMapper _vehicleModelMapper;

        public VehicleModelController(IVehicleModelService vehicleModelService, IManufacturerService manufacturerService, VehicleModelMapper vehicleModelMapper)
        {
            _vehicleModelService = vehicleModelService;
            _manufacturerService = manufacturerService;
            _vehicleModelMapper = vehicleModelMapper;
        }

        /// <summary>
        /// Prikazuje listu modela vozila.
        /// </summary>
        /// <returns></returns>
        [CustomAuthorization]
        public ActionResult Index()
        {
            var vehicleModelDomainModels = _vehicleModelService.GetAll();
            var vehicleModels = _vehicleModelMapper.EntityMapper.Map<IList<VehicleModel>>(vehicleModelDomainModels);
            foreach (var vehicleModel in vehicleModels)
            {
                vehicleModel.Manufacturer = _manufacturerService.GetById(vehicleModel.ManufacturerId).Name;
            }
            return View(vehicleModels);
        }

        /// <summary>
        /// Prikazuje formu za unos novog modela vozila.
        /// </summary>
        /// <returns></returns>
        [CustomAuthorization]
        [HttpGet]
        public ActionResult Create()
        {
            var manufacturers = _manufacturerService.GetAll();
            var vehicleTypes = Enum.GetValues(typeof(VehicleType)).Cast<VehicleType>().ToList(); 
            var fuels = Enum.GetValues(typeof(Fuel)).Cast<Fuel>().ToList();

            ViewBag.Proizvodjaci = manufacturers.Select(x =>
                        new SelectListItem
                        {
                            Value = x._id.ToString(),
                            Text = x.Name
                        });

            ViewBag.TipoviVozila = vehicleTypes.Select(x =>
                        new SelectListItem
                        {
                            Value = ((int)x).ToString(),
                            Text = x.ToString()
                        });

            ViewBag.TipoviGoriva = fuels.Select(x =>
                        new SelectListItem
                        {
                            Value = ((int)x).ToString(),
                            Text = x.ToString()
                        }); 
            return View();
        }

        /// <summary>
        /// Kreira novi model vozila.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [CustomAuthorization]
        [HttpPost]
        public ActionResult Create(VehicleModelDomainModel model)
        {
            if (ModelState.IsValid)
            {
                if(_vehicleModelService.GetByName(model.Name) != null)
                {
                    ModelState.AddModelError("", "Unešeni model vozila već postoji.");
                }
                else
                {
                    _vehicleModelService.Save(model);
                    return RedirectToAction("Index", "Vehicle");
                }
                
            }

            var manufacturers = _manufacturerService.GetAll();
            var vehicleTypes = Enum.GetValues(typeof(VehicleType)).Cast<VehicleType>().ToList();
            var fuels = Enum.GetValues(typeof(Fuel)).Cast<Fuel>().ToList();

            ViewBag.Proizvodjaci = manufacturers.Select(x =>
                        new SelectListItem
                        {
                            Value = x._id.ToString(),
                            Text = x.Name
                        });

            ViewBag.TipoviVozila = vehicleTypes.Select(x =>
                        new SelectListItem
                        {
                            Value = ((int)x).ToString(),
                            Text = x.ToString()
                        });

            ViewBag.TipoviGoriva = fuels.Select(x =>
                        new SelectListItem
                        {
                            Value = ((int)x).ToString(),
                            Text = x.ToString()
                        });

            return View(model);
        }

        /// <summary>
        /// Prikazuje formu za ažuriranje postojećeg vozila.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [CustomAuthorization]
        [HttpGet]
        public ActionResult Edit(Guid ID)
        {
            var manufacturers = _manufacturerService.GetAll();
            var vehicleTypes = Enum.GetValues(typeof(VehicleType)).Cast<VehicleType>().ToList();
            var fuels = Enum.GetValues(typeof(Fuel)).Cast<Fuel>().ToList();

            ViewBag.Proizvodjaci = manufacturers.Select(x =>
                        new SelectListItem
                        {
                            Value = x._id.ToString(),
                            Text = x.Name
                        });

            ViewBag.TipoviVozila = vehicleTypes.Select(x =>
                        new SelectListItem
                        {
                            Value = ((int)x).ToString(),
                            Text = x.ToString()
                        });

            ViewBag.TipoviGoriva = fuels.Select(x =>
                        new SelectListItem
                        {
                            Value = ((int)x).ToString(),
                            Text = x.ToString()
                        });

            var model = _vehicleModelService.GetById(ID);

            return View(model);
        }

        /// <summary>
        /// Ažurira postojeći model vozila.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [CustomAuthorization]
        [HttpPost]
        public ActionResult Edit(VehicleModelDomainModel model)
        {
            if (ModelState.IsValid)
            {
                _vehicleModelService.Update(model);
                return RedirectToAction("Index", "Vehicle");
            }
            return View(model);
        }

        /// <summary>
        /// Briše model vozila prema ID-u.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [CustomAuthorization]
        public JsonResult Delete(Guid ID)
        {
            _vehicleModelService.Delete(ID);

            return Json("OK");
        }

    }
}