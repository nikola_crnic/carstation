﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CarStation.Domain.Models;
using CarStation.Domain.Services;
using CarStation.Presentation.Models;

namespace CarStation.Presentation.Controllers
{
    public class VehicleController : Controller
    {
        private readonly IVehicleService _vehicleService;
        private readonly VehicleMapper _vehicleMapper;
        private readonly IVehicleModelService _vehicleModelService;
        private readonly IBranchOfficeService _branchOfficeService;
        private readonly IManufacturerService _manufacturerService;
        private ILocationService _locationService;
        private ICurrencyService _currencyService;

        public VehicleController(IVehicleService vehicleService, IVehicleModelService vehicleModelService, IBranchOfficeService branchOfficeService, VehicleMapper vehicleMapper, 
            IManufacturerService manufacturerService, ILocationService locationService, ICurrencyService currencyService)
        {
            _vehicleService = vehicleService;
            _vehicleModelService = vehicleModelService;
            _branchOfficeService = branchOfficeService;
            _vehicleMapper = vehicleMapper;
            _manufacturerService = manufacturerService;
            _locationService = locationService;
            _currencyService = currencyService;
        }

        /// <summary>
        /// Prikazuje vozila, proizvođače i modele vozila.
        /// </summary>
        /// <returns></returns>
        [CustomAuthorization]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Prikazuje listu vozila.
        /// </summary>
        /// <returns></returns>
        [CustomAuthorization]
        public ActionResult List()
        {
            var vehicles = _vehicleService.GetAll();
            
            var model = _vehicleMapper.EntityMapper.Map<IList<Vehicle>>(vehicles);
            foreach (var vehicle in model)
            {
                var vehicleModel = _vehicleModelService.GetById(vehicle.VehicleModelId);
                var branchOffice = _branchOfficeService.GetById(vehicle.BranchOfficeId);
                var location = _locationService.GetById(branchOffice.LocationId);
                var currency = _currencyService.GetById(location.CurrencyId);
                var manufacturer = _manufacturerService.GetById(vehicleModel.ManufacturerId);

                vehicle.Manufacturer = manufacturer.Name;
                vehicle.VehicleModel = vehicleModel.Name;
                vehicle.Currency = currency.code;
            }
            return View(model);
        }

        [HttpGet]
        public JsonResult GetBranchOfficeVehicles(Guid ID)
        {
            var model = from v in _vehicleService.GetByBranchOffice(ID)
                join m in _vehicleModelService.GetAll() on v.VehicleModelId equals m._id
                join z in _manufacturerService.GetAll() on m.ManufacturerId equals z._id
                select new
                {
                    id = v._id.ToString(),
                    text = z.Name + " " + m.Name
                };

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetAll()
        {
            var model = from v in _vehicleService.GetAll()
                        join m in _vehicleModelService.GetAll() on v.VehicleModelId equals m._id
                        join z in _manufacturerService.GetAll() on m.ManufacturerId equals z._id
                        select new
                        {
                            id = v._id.ToString(),
                            text = z.Name + " " + m.Name
                        };

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Prikazuje formu za unos novog vozila.
        /// </summary>
        /// <returns></returns>
        [CustomAuthorization]
        [HttpGet]
        public ActionResult Create()
        {
            var vehicleModels = _vehicleModelService.GetAll();
            var branchOffices = _branchOfficeService.GetAll();
            ViewBag.ModeliVozila = vehicleModels.Select(x =>
                        new SelectListItem
                        {
                            Value = x._id.ToString(),
                            Text = x.Name
                        });

            ViewBag.Filijale = branchOffices.Select(x =>
                        new SelectListItem
                        {
                            Value = x._id.ToString(),
                            Text = x.Name
                        }); 
            return View();
        }

        /// <summary>
        /// Kreira novo vozilo.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [CustomAuthorization]
        [HttpPost]
        public ActionResult Create(VehicleDomainModel model)
        {
            if (ModelState.IsValid)
            {
                if (_vehicleService.GetByName(model.RegistrationNumber) != null)
                {
                    ModelState.AddModelError("", "Vozilo sa registarskim brojem " + model.RegistrationNumber + " već postoji.");
                } 
                else
                {
                    _vehicleService.Save(model);
                    return RedirectToAction("Index");
                }
            }

            var vehicleModels = _vehicleModelService.GetAll();
            var branchOffices = _branchOfficeService.GetAll();
            ViewBag.ModeliVozila = vehicleModels.Select(x =>
                        new SelectListItem
                        {
                            Value = x._id.ToString(),
                            Text = x.Name
                        });

            ViewBag.Filijale = branchOffices.Select(x =>
                        new SelectListItem
                        {
                            Value = x._id.ToString(),
                            Text = x.Name
                        });

            return View(model);
        }

        /// <summary>
        /// Prikazuje detalje traženog vozila.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult Details(Guid ID)
        {
            var vehicle = _vehicleService.GetById(ID);

            var model = _vehicleMapper.EntityMapper.Map<Vehicle>(vehicle);
            var vehicleModel = _vehicleModelService.GetById(model.VehicleModelId);
            model.Fuel = vehicleModel.Fuel.ToString();
            model.Type = vehicleModel.Type.ToString();
            model.Status = vehicleModel.Status.ToString();
            model.Passengers = vehicleModel.Passengers;
            model.IsAutomatic = vehicleModel.IsAutomatic;
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Prikazuje formu za ažuriranje postojećeg vozila.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [CustomAuthorization]
        [HttpGet]
        public ActionResult Edit(Guid ID)
        {
            var vehicleModels = _vehicleModelService.GetAll();
            var branchOffices = _branchOfficeService.GetAll();
            ViewBag.ModeliVozila = vehicleModels.Select(x =>
                        new SelectListItem
                        {
                            Value = x._id.ToString(),
                            Text = x.Name
                        });

            ViewBag.Filijale = branchOffices.Select(x =>
                        new SelectListItem
                        {
                            Value = x._id.ToString(),
                            Text = x.Name
                        });

            var model = _vehicleService.GetById(ID);
            return View(model);
        }

        /// <summary>
        /// Ažurira postojeće vozilo.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [CustomAuthorization]
        [HttpPost]
        public ActionResult Edit(VehicleDomainModel model)
        {
            if (ModelState.IsValid)
            {
                if (_vehicleService.GetById(model._id).Picture != null && model.Picture != null)
                    DeletePicture(_vehicleService.GetById(model._id).Picture);

                _vehicleService.Update(model);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        /// <summary>
        /// Briše vozilo prema ID-u.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [CustomAuthorization]
        public JsonResult Delete(Guid ID)
        {
            var vehicle = _vehicleService.GetById(ID);

            _vehicleService.Delete(ID);

            if (vehicle.Picture != null)
                DeletePicture(vehicle.Picture);

            return Json("OK");
        }

        /// <summary>
        /// Čuva sliku za traženo vozilo.
        /// </summary>
        /// <returns></returns>
        [CustomAuthorization]
        [HttpPost]
        public JsonResult SavePicture()
        {
            for (int i = 0; i < Request.Files.Count; i++)
            {
                var file = Request.Files[i];

                var fileName = Path.GetFileName(file.FileName);
                var path = "/Slike/Vozila/" + fileName;
                var fullPath = Server.MapPath(path);
                file.SaveAs(fullPath);

                return Json(path, JsonRequestBehavior.AllowGet);
            }
            return Json("ERROR");
        }

        /// <summary>
        /// Briše sliku za traženo vozilo.
        /// </summary>
        /// <param name="Picture"></param>
        [CustomAuthorization]
        [HttpPost]
        public void DeletePicture(string Picture)
        {
            string fullPath = Request.MapPath(Picture);
            if (System.IO.File.Exists(fullPath))
            {
                System.IO.File.Delete(fullPath);
            }
        }

        /// <summary>
        /// Lista najčešće iznajmljivanih vozila.
        /// </summary>
        /// <param name="Number"></param>
        /// <returns></returns>
        [CustomAuthorization]
        [HttpGet]
        public JsonResult TopVehicles(int Number)
        {
            var vehicles = _vehicleService.GetTopVehicles(Number);
            return Json(vehicles, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// Lista najčešće iznajmljivanih vozila.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GetVehicleModel(Guid VehicleId)
        {
            var vehicle = _vehicleService.GetById(VehicleId);
            var vehicleModel = _vehicleModelService.GetById(vehicle.VehicleModelId);
            return Json(vehicleModel, JsonRequestBehavior.AllowGet);
        }
    }
}