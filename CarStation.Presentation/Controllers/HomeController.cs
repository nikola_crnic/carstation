﻿
using CarStation.Presentation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CarStation.Domain.Services;

namespace CarStation.Presentation.Controllers
{
    public class HomeController : Controller
    {
        private readonly IRentService _rentService;
        private readonly ILocationService _locationService;
        private readonly ICountryService _countryService;
        private readonly IBranchOfficeService _branchOfficeService;
        private readonly BranchOfficeMapper _branchOfficeMapper;

        public HomeController(IRentService rentService, ILocationService locationService, BranchOfficeMapper branchOfficeMapper, 
            IBranchOfficeService branchOfficeService, ICountryService countryService)
        {
            _rentService = rentService;
            _locationService = locationService;
            _branchOfficeMapper = branchOfficeMapper;
            _branchOfficeService = branchOfficeService;
            _countryService = countryService;
        }

        /// <summary>
        /// Prikazuje listu gradova.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            var locations = _locationService.GetAll();
            ViewBag.Gradovi = locations.Select(x =>
                        new SelectListItem
                        {
                            Value = x._id.ToString(),
                            Text = $"({_countryService.GetById(x.CountryId).code}) {x.Name}"
                        }).OrderBy(x => x.Text);
            return View();
        }

        /// <summary>
        /// Prikazuje stranicu o nama.
        /// </summary>
        /// <returns></returns>
        public ActionResult About()
        {   
            return View();
        }

        /// <summary>
        /// Prikazuje kontakt stranicu.
        /// </summary>
        /// <returns></returns>
        public ActionResult Contact()
        {
            var officeDomainModels = _branchOfficeService.GetAll();
            var offices = _branchOfficeMapper.EntityMapper.Map<IList<BranchOffice>>(officeDomainModels);
            foreach (var office in offices)
            {
                office.LocationName = _locationService.GetById(office.LocationId).Name;
            }
            ViewBag.Filijale = offices;

            return View();
        }

        /// <summary>
        /// Prikazuje kontrolnu tablu.
        /// </summary>
        /// <returns></returns>
        [CustomAuthorization]
        [HttpGet]
        public ActionResult Dashboard()
        {
            return View();
        }

        /// <summary>
        /// Statistički prikaz zakupa po mjesecima.
        /// </summary>
        /// <returns></returns>
        [CustomAuthorization]
        [HttpGet]
        public JsonResult RentStatistics()
        {
            var rents = _rentService.GetByMonth();
            return Json(rents, JsonRequestBehavior.AllowGet);
        }
    }
}