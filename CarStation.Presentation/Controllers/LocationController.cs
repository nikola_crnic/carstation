﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CarStation.Domain.Models;
using CarStation.Domain.Services;
using CarStation.Presentation.Models;

namespace CarStation.Presentation.Controllers
{
    public class LocationController : Controller
    {
        private readonly ILocationService _locationService;
        private readonly LocationMapper _locationMapper;
        private readonly ICountryService _countryService;
        private readonly ICurrencyService _currencyService;

        public LocationController(ILocationService locationService, LocationMapper locationMapper, ICountryService countryService, ICurrencyService currencyService)
        {
            _locationService = locationService;
            _locationMapper = locationMapper;
            _countryService = countryService;
            _currencyService = currencyService;
        }

        /// <summary>
        /// Prikazuje listu gradova.
        /// </summary>
        /// <returns></returns>
        [CustomAuthorization]
        public ActionResult Index()
        {
            var locationDomainModels = _locationService.GetAll();

            var locations = _locationMapper.EntityMapper.Map<IList<Location>>(locationDomainModels);

            foreach (var location in locations)
            {
                location.Country = _countryService.GetById(location.CountryId).name;
                location.Currency = $"{_currencyService.GetById(location.CurrencyId).name} ({_currencyService.GetById(location.CurrencyId).code})";
            }

            return View(locations);
        }

        /// <summary>
        /// Prikazuje formu za kreiranje novog lokacije.
        /// </summary>
        /// <returns></returns>
        [CustomAuthorization]
        [HttpGet]
        public ActionResult Create()
        {
            var countries = _countryService.GetAll();
            ViewBag.Drzave = countries.Select(x =>
                        new SelectListItem
                        {
                            Value = x._id.ToString(),
                            Text = $"({x.code})  {x.name}"
                        });
            var currencies = _currencyService.GetAll();
            ViewBag.Valute = currencies.Select(x =>
                        new SelectListItem
                        {
                            Value = x._id.ToString(),
                            Text = $"({x.code})  {x.name}"
                        });
            return View();
        }

        /// <summary>
        /// Kreira novi Location.
        /// </summary>
        /// <param name="isModal"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [CustomAuthorization]
        [HttpPost]
        public ActionResult Create(bool? isModal, LocationDomainModel model)
        {
            if (ModelState.IsValid)
            {
                if(_locationService.GetByName(model.Name) != null)
                {
                    ModelState.AddModelError("", "Unešena lokacija već postoji.");
                    return View(model);
                }
                _locationService.Save(model);
                return isModal.HasValue ? (ActionResult) Json("OK") : RedirectToAction("Index");
            }
            return View(model);
        }

        /// <summary>
        /// Prikazuje formu za ažuriranje postojećeg lokacije.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [CustomAuthorization]
        [HttpGet]
        public ActionResult Edit(Guid ID)
        {
            var countries = _countryService.GetAll();
            ViewBag.Drzave = countries.Select(x =>
                        new SelectListItem
                        {
                            Value = x._id.ToString(),
                            Text = $"({x.code})  {x.name}"
                        });
            var currencies = _currencyService.GetAll();
            ViewBag.Valute = currencies.Select(x =>
                        new SelectListItem
                        {
                            Value = x._id.ToString(),
                            Text = $"({x.code})  {x.name}"
                        });
            var location = _locationService.GetById(ID);
            return View(location);
        }

        /// <summary>
        /// Ažurira postojeći Location.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [CustomAuthorization]
        [HttpPost]
        public ActionResult Edit(LocationDomainModel model)
        {
            if (ModelState.IsValid)
            {
                _locationService.Update(model);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        /// <summary>
        /// Briše Location prema ID-u.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [CustomAuthorization]
        public JsonResult Delete(Guid ID)
        {
            _locationService.Delete(ID);

            return Json("OK");
        }
    }
}