﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security;
using System.Security.Permissions;
using System.Web;
using System.Web.ModelBinding;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using CarStation.Domain.Models;
using CarStation.Domain.Models.Enums;
using CarStation.Domain.Services;
using CarStation.Presentation.Models;
using Microsoft.Reporting.WebForms;

namespace CarStation.Presentation.Controllers
{
    public class RentController : Controller
    {
        private readonly IRentService _rentService;
        private readonly ILocationService _locationService;
        private readonly IClientService _clientService;
        private readonly IBranchOfficeService _branchOfficeService;
        private readonly IVehicleService _vehicleService;
        private readonly RentMapper _rentMapper;
        private readonly IVehicleModelService _vehicleModelService;
        private readonly IManufacturerService _manufacturerService;
        private readonly ICurrencyService _currencyService;

        public RentController(IRentService rentService, RentMapper rentMapper, ILocationService locationService, IClientService clientService, 
            IBranchOfficeService branchOfficeService, IVehicleService vehicleService, IVehicleModelService vehicleModelService, 
            IManufacturerService manufacturerService, ICurrencyService currencyService)
        {
            _rentService = rentService;
            _rentMapper = rentMapper;
            _locationService = locationService;
            _clientService = clientService;
            _branchOfficeService = branchOfficeService;
            _vehicleService = vehicleService;
            _vehicleModelService = vehicleModelService;
            _manufacturerService = manufacturerService;
            _currencyService = currencyService;
        }

        /// <summary>
        /// Prikazuje listu zakupa.
        /// </summary>
        /// <returns></returns>
        [CustomAuthorization]
        public ActionResult Index()
        {
            var rentDomainModels = _rentService.GetAll();
            var rents = _rentMapper.EntityMapper.Map<IList<Rent>>(rentDomainModels);
            foreach (var rent in rents)
            {
                var branchOffice = _branchOfficeService.GetById(rent.OfficeId);
                var location = _locationService.GetById(branchOffice.LocationId);
                rent.Currency = _currencyService.GetById(location.CurrencyId).symbol_native;
                rent.TotalNetAmount = _rentService.CalculateTotalNetAmount(rent._id);
                rent.TotalGrossAmount = _rentService.CalculateTotalGrossAmount(rent._id);
            }
            return View(rents);
        }

        [HttpGet]
        public ActionResult Create()
        {
            var locations = _locationService.GetAll();
            ViewBag.Gradovi = locations.Select(x =>
                        new SelectListItem
                        {
                            Value = x._id.ToString(),
                            Text = x.Name
                        });
            return View();
        }

        [HttpPost]
        public JsonResult Create(RentDomainModel model)
        {
            if (ModelState.IsValid)
            {
                var branchOffice = _branchOfficeService.GetById(model.OfficeId);
                var location = _locationService.GetById(branchOffice.LocationId);
                model.Tax = location.Tax;
                _rentService.Save(model);

                if(model.Status == RentStatus.New)
                {
                    var client = _clientService.GetById(model.ClientId.Value);
                    var vehicle = _vehicleService.GetById(model.VehicleId.Value);
                    var vehicleModel = _vehicleModelService.GetById(vehicle.VehicleModelId);
                    var manufacturer = _manufacturerService.GetById(vehicleModel.ManufacturerId);

                    var invoice = new Invoice
                    {
                        RentFrom = model.RentedFrom,
                        RentTo = model.RentedTo,
                        Email = client.Email,
                        OfficeStreet = branchOffice.Street,
                        OfficeName = branchOffice.Name,
                        OfficeTelephone = branchOffice.Telephone,
                        YearOfProduction = vehicle.YearOfProduction,
                        LocationName = location.Name,
                        Name = client.Name,
                        ClientTelephone = client.Telephone,
                        ModelName = vehicleModel.Name,
                        ManufacturerName = manufacturer.Name,
                        RegistrationNumber = vehicle.RegistrationNumber,
                        RentNumber = model.RentNumber,
                        Total = model.TotalNetAmount,
                        PricePerDay = vehicle.PricePerDay,
                        Tax = model.Tax
                    };

                    var email = new Email
                    {
                        To = client.Email,
                        Body = RenderPartialViewToString("Invoice", invoice),
                        Subject = "Potvrda rezervacije"
                    };

                    SendEmail(email, null);
                }

                return Json("OK");
            }

            return Json("ERROR");
        }

        /// <summary>
        /// Prikazuje formu za ažuriranje postojećeg zakupa.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Edit(Guid ID)
        {
            var locations = _locationService.GetAll();
            ViewBag.Gradovi = locations.Select(x =>
                        new SelectListItem
                        {
                            Value = x._id.ToString(),
                            Text = x.Name
                        });
            var model = _rentService.GetById(ID);

            return View(model);
        }

        /// <summary>
        /// Ažurira postojeći zakup.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Edit(RentDomainModel model)
        {
            if (ModelState.IsValid)
            {
                model.Status = RentStatus.NotPaid;
                _rentService.Update(model);
                model.TotalNetAmount = _rentService.CalculateTotalNetAmount(model._id);
                model.TotalGrossAmount = _rentService.CalculateTotalGrossAmount(model._id);
                _rentService.Update(model);

                var client = _clientService.GetById(model.ClientId.Value);
                var branchOffice = _branchOfficeService.GetById(model.OfficeId);
                var location = _locationService.GetById(branchOffice.LocationId);
                var vehicle = _vehicleService.GetById(model.VehicleId.Value);
                var vehicleModel = _vehicleModelService.GetById(vehicle.VehicleModelId);
                var manufacturer = _manufacturerService.GetById(vehicleModel.ManufacturerId);
                var currency = _currencyService.GetById(location.CurrencyId);

                vehicleModel.Status = VehicleStatus.Rented;
                _vehicleModelService.Update(vehicleModel);

                model = _rentService.GetById(model._id);

                var invoice = new Invoice
                {
                    RentFrom = model.RentedFrom,
                    RentTo = model.RentedTo,
                    Email = client.Email,
                    OfficeStreet = branchOffice.Street,
                    OfficeName = branchOffice.Name,
                    OfficeTelephone = branchOffice.Telephone,
                    YearOfProduction = vehicle.YearOfProduction,
                    LocationName = location.Name,
                    Name = client.Name,
                    ClientTelephone = client.Telephone,
                    ModelName = vehicleModel.Name,
                    ManufacturerName = manufacturer.Name,
                    RegistrationNumber = vehicle.RegistrationNumber,
                    RentNumber = model.RentNumber,
                    Total = model.TotalGrossAmount,
                    TotalNetAmount = model.TotalNetAmount,
                    PricePerDay = vehicle.PricePerDay,
                    Tax = location.Tax,
                    Currency = currency.symbol_native
                };

                var email = new Email
                {
                    To = client.Email,
                    Body = RenderPartialViewToString("Invoice", invoice),
                    Subject = "Potvrda rezervacije"
                };

                SendEmail(email, null);


                return Json("OK");
            }
            return Json("ERROR");
        }

        protected string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }

        /// <summary>
        /// Briše zakup prema ID-u.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public JsonResult Delete(Guid ID)
        {
            try
            {
                _rentService.Delete(ID);
            }
            catch
            {
                return Json("ERROR");
            }
            

            return Json("OK");
        }

        /// <summary>
        /// Mijenja status zakupa prema ID-u.
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public JsonResult UpdateStatus(Guid ID, int status)
        {
            var rent = _rentService.GetById(ID);
            var vehicle = _vehicleService.GetById(rent.VehicleId.Value);
            var vehicleModel = _vehicleModelService.GetById(vehicle.VehicleModelId);
            try
            {
                vehicleModel.Status = VehicleStatus.Available;
                _vehicleModelService.Update(vehicleModel);

                rent.Status = (Domain.Models.Enums.RentStatus)status;
                _rentService.Update(rent);
            }
            catch
            {
                return Json("ERROR");
            }
            

            return Json("OK");
        }

        /// <summary>
        /// Šalje email klijentu sa fakturom zakupa.
        /// </summary>
        /// <param name="email"></param>
        /// <param name="file"></param>
        [HttpPost]
        public void SendEmail(Email email, byte[] file)
        {
            string from = "rentacarnc@gmail.com"; //example:- name@gmail.com
            using (MailMessage mail = new MailMessage(from, email.To))
            {
                mail.Subject = email.Subject;
                mail.Body = email.Body;
                if (file != null)
                {
                    Stream stream = new MemoryStream(file);
                    string fileName = "faktura.pdf";
                    mail.Attachments.Add(new Attachment(stream, fileName));
                }
                mail.CC.Add(from);
                mail.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    EnableSsl = true
                };
                NetworkCredential networkCredential = new NetworkCredential(from, "rentacarnc1");
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = networkCredential;
                smtp.Port = 587;
                smtp.Send(mail);
                ViewBag.Message = "Sent";
            }
        }

        /// <summary>
        /// Lista zakupa koji imaju status "u procesu".
        /// </summary>
        /// <returns></returns>
        public JsonResult RentsInProcess()
        {
            var rents = _rentService.GetInProcess()
                .Select(x => new Rent
                {
                    RentedFrom = x.RentedFrom,
                    RentedTo = x.RentedTo,
                    TotalGrossAmount = x.TotalGrossAmount,
                    RentNumber = x.RentNumber,
                    _id = x._id
                });

            return Json(rents, JsonRequestBehavior.AllowGet);
        }

    }
}