﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CarStation.Domain.Models;
using CarStation.Domain.Services;
using CarStation.Presentation.Models;

namespace CarStation.Presentation.Controllers
{
    public class ManufacturerController : Controller
    {
        private readonly IManufacturerService _manufacturerService;
        private readonly ManufacturerMapper _manufacturerMapper;

        public ManufacturerController(IManufacturerService manufacturerService, ManufacturerMapper manufacturerMapper)
        {
            _manufacturerService = manufacturerService;
            _manufacturerMapper = manufacturerMapper;
        }

        /// <summary>
        /// Prikazuje listu proizvođača.
        /// </summary>
        /// <returns></returns>
        [CustomAuthorization]
        public ActionResult Index()
        {
            var manufacturerDomainModels = _manufacturerService.GetAll();
            var manufacturers = _manufacturerMapper.EntityMapper.Map<IList<Manufacturer>>(manufacturerDomainModels);
            return View(manufacturers);
        }

        /// <summary>
        /// Prikazuje formu za unos novog proizvođača.
        /// </summary>
        /// <returns></returns>
        [CustomAuthorization]
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        /// <summary>
        /// Kreira novog proizvođača.
        /// </summary>
        /// <param name="isModal"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [CustomAuthorization]
        [HttpPost]
        public ActionResult Create(bool? isModal, ManufacturerDomainModel model)
        {
            if (ModelState.IsValid)
            {
                if(_manufacturerService.GetByName(model.Name) != null)
                {
                    ModelState.AddModelError("", "Unešeni proizvođač već postoji");
                    return View(model);
                }

                _manufacturerService.Save(model);

                return isModal.HasValue && isModal.Value
                    ? (ActionResult) Json("OK")
                    : RedirectToAction("Index", "Vehicle");
            }
            return View(model);
        }

        /// <summary>
        /// Prikazuje formu za ažuriranje postojećeg proizvođača.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [CustomAuthorization]
        [HttpGet]
        public ActionResult Edit(Guid ID)
        {
            var model = _manufacturerService.GetById(ID);
            return View(model);
        }

        /// <summary>
        /// Ažurira postojećeg proizvođača.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [CustomAuthorization]
        [HttpPost]
        public ActionResult Edit(ManufacturerDomainModel model)
        {
            if (ModelState.IsValid)
            {
                if (_manufacturerService.GetById(model._id).Logo != null && model.Logo != null)
                    DeleteLogo(_manufacturerService.GetById(model._id).Logo);

                _manufacturerService.Update(model);
                return RedirectToAction("Index", "Vehicle");
            }
            return View(model);
        }

        /// <summary>
        /// Briše proizvođača prema ID-u.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [CustomAuthorization]
        public JsonResult Delete(Guid ID)
        {
            var manufacturer = _manufacturerService.GetById(ID);
            _manufacturerService.Delete(ID);

            if (manufacturer.Logo != null)
                DeleteLogo(manufacturer.Logo);

            return Json("OK");
        }

        /// <summary>
        /// Čuva logo za traženog proizvođača.
        /// </summary>
        /// <returns></returns>
        [CustomAuthorization]
        [HttpPost]
        public JsonResult SavePicture()
        {
            for (int i = 0; i < Request.Files.Count; i++)
            {
                var file = Request.Files[i];

                var fileName = Path.GetFileName(file.FileName);
                var path = "/Slike/Proizvodjaci/" + fileName;
                var fullPath = Server.MapPath(path);
                file.SaveAs(fullPath);

                return Json(path, JsonRequestBehavior.AllowGet);
            }
            return Json("ERROR");
        }

        /// <summary>
        /// Briše logo za traženog proizvođača.
        /// </summary>
        /// <param name="Logo"></param>
        [CustomAuthorization]
        [HttpPost]
        public void DeleteLogo(string Logo)
        {
            string fullPath = Request.MapPath(Logo);
            if (System.IO.File.Exists(fullPath))
            {
                System.IO.File.Delete(fullPath);
            }
        }
    }
}