﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using CarStation.Domain.Models;
using CarStation.Domain.Services;
using CarStation.Presentation.Models;
using BranchOffice = CarStation.Presentation.Models.BranchOffice;

namespace CarStation.Presentation.Controllers
{
    public class BranchOfficeController : Controller
    {
        private readonly IBranchOfficeService _branchOfficeService;
        private readonly ILocationService _locationService;
        private readonly BranchOfficeMapper _branchOfficeMapper;
        private readonly ICountryService _countryService;
        private ICurrencyService _currencyService;

        public BranchOfficeController(IBranchOfficeService branchOfficeService, ILocationService locationService, BranchOfficeMapper branchOfficeMapper, ICountryService countryService, ICurrencyService currencyService)
        {
            _branchOfficeService = branchOfficeService;
            _locationService = locationService;
            _branchOfficeMapper = branchOfficeMapper;
            _countryService = countryService;
            _currencyService = currencyService;
        }

        /// <summary>
        /// Prikazuje listu filijala.
        /// </summary>
        /// <returns></returns>
        [CustomAuthorization]
        [Authorize]
        public ActionResult Index()
        {
            var branchDomainModels = _branchOfficeService.GetAll();
            var offices = _branchOfficeMapper.EntityMapper.Map<IList<BranchOffice>>(branchDomainModels);
            foreach (var branchOffice in offices)
            {
                branchOffice.LocationName = _locationService.GetById(branchOffice.LocationId).Name;
            }
            return View(offices);
        }

        [HttpGet]
        public JsonResult GetByCity(Guid ID)
        {
            var branchOffices = _branchOfficeService.GetByCity(ID);

            var model = branchOffices.Select(x =>
                        new
                        {
                            id = x._id.ToString(),
                            text = x.Name
                        });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Prikazuje formu za unos nove filijale.
        /// </summary>
        /// <returns></returns>
        [CustomAuthorization]
        [HttpGet]
        public ActionResult Create()
        {
            var locations = _locationService.GetAll();
            ViewBag.Gradovi = locations.Select(x =>
                        new SelectListItem
                        {
                            Value = x._id.ToString(),
                            Text = x.Name
                        });

            var countries = _countryService.GetAll();
            ViewBag.Drzave = countries.Select(x =>
                        new SelectListItem
                        {
                            Value = x._id.ToString(),
                            Text = $"({x.code})  {x.name}" 
                        });
            var currencies = _currencyService.GetAll();
            ViewBag.Valute = currencies.Select(x =>
                        new SelectListItem
                        {
                            Value = x._id.ToString(),
                            Text = $"({x.code})  {x.name}"
                        });
            return View();
        }

        /// <summary>
        /// Kreira novu filijalu.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [CustomAuthorization]
        [HttpPost]
        public ActionResult Create(BranchOfficeDomainModel model)
        {
            if (ModelState.IsValid)
            {
                var existingBranchOffice = _branchOfficeService.GetByName(model.Name);

                if(existingBranchOffice != null)
                {
                    ModelState.AddModelError("", "Filijala sa unešenim Nameom već postoji.");
                }
                else
                {
                    _branchOfficeService.Save(model);
                    return RedirectToAction("Index");
                }
            }

            var locations = _locationService.GetAll();
            ViewBag.Gradovi = locations.Select(x =>
                        new SelectListItem
                        {
                            Value = x._id.ToString(),
                            Text = x.Name
                        }); 

            return View(model);
        }

        /// <summary>
        /// Prikazuje formu za ažuriranje filijale.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [CustomAuthorization]
        [HttpGet]
        public ActionResult Edit(Guid ID)
        {
            var locations = _locationService.GetAll();
            ViewBag.Gradovi = locations.Select(x =>
                        new SelectListItem
                        {
                            Value = x._id.ToString(),
                            Text = x.Name
                        });

            var countries = _countryService.GetAll();
            ViewBag.Drzave = countries.Select(x =>
                        new SelectListItem
                        {
                            Value = x._id.ToString(),
                            Text = $"({x.code})  {x.name}"
                        });
            var currencies = _currencyService.GetAll();
            ViewBag.Valute = currencies.Select(x =>
                        new SelectListItem
                        {
                            Value = x._id.ToString(),
                            Text = $"({x.code})  {x.name}"
                        });

            var branchOffice = _branchOfficeService.GetById(ID);

            return View(branchOffice);
        }

        /// <summary>
        /// Ažurira postojeću filijalu.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [CustomAuthorization]
        [HttpPost]
        public ActionResult Edit(BranchOfficeDomainModel model)
        {
            if (!ModelState.IsValid) return View(model);

            _branchOfficeService.Update(model);
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Briše filijalu prema njenom ID-u.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [CustomAuthorization]
        public JsonResult Delete(Guid ID)
        {
            var branchOffice = _branchOfficeService.GetById(ID);
            _branchOfficeService.Delete(branchOffice._id);

            return Json("OK");
        }

        /// <summary>
        /// Prikazuje promet prema filijalama.
        /// </summary>
        /// <returns></returns>
        [CustomAuthorization]
        [HttpGet]
        public JsonResult GetNumberOfRentsByBranchOffice()
        {
            var filijale = _branchOfficeService.GetNumberOfRentsByBranchOffice();
            return Json(filijale, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetLocationCurrency(Guid officeId)
        {
            var office = _branchOfficeService.GetById(officeId);
            var location = _locationService.GetById(office.LocationId);
            var currency = _currencyService.GetById(location.CurrencyId);

            return Json(currency.symbol_native, JsonRequestBehavior.AllowGet);
        }
    }
}