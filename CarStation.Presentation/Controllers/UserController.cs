﻿
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI.WebControls;
using CarStation.Domain.Models;
using CarStation.Domain.Services;
using CarStation.Presentation.Models;

namespace CarStation.Presentation.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        /// <summary>
        /// Prikazuje formu za prijavu.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Login()
        {
            //InitRegistration();
            if (User.Identity.IsAuthenticated)
                return RedirectToAction("Dashboard", "Home");
            return View();
        }

        /// <summary>
        /// Prijavljuje korisnika na sistem.
        /// </summary>
        /// <param name="korisnik"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Login(User user)
        {
            if(ModelState.IsValid)
            {
                if (IsValid(user.Email, user.Password))
                {
                    FormsAuthentication.SetAuthCookie(user.Email, false);
                    return RedirectToAction("Dashboard", "Home");
                }
                else
                    ModelState.AddModelError("", "Pogrešan email ili lozinka.");
            }
            return View(user);
        }

        public void InitRegistration()
        {
            var crypto = new SimpleCrypto.PBKDF2();
            var cryptoLozinka = crypto.Compute("P@ssw0rd");

            var model = new UserDomainModel
            {
                Email = "crnic.nikola@gmail.com",
                Password = cryptoLozinka,
                PasswordSalt = crypto.Salt
            };

            _userService.Save(model);
        }

        /// <summary>
        /// Odjavljuje korisnika sa sistema.
        /// </summary>
        /// <returns></returns>
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Provjerava da li su unešeni podaci za prijavu na sistem ispravni.
        /// </summary>
        /// <param name="Email"></param>
        /// <param name="Lozinka"></param>
        /// <returns></returns>
        public bool IsValid(string Email, string Lozinka)
        {
            bool isValid = false;

            var crypto = new SimpleCrypto.PBKDF2();

            var korisnik = _userService.GetByEmail(Email);

            if(korisnik != null)
            {
                if (korisnik.Password == crypto.Compute(Lozinka, korisnik.PasswordSalt))
                    isValid = true;
            }

            return isValid;
        }
    }
}