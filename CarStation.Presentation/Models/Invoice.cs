﻿namespace CarStation.Presentation.Models
{
    public class Invoice
    {
        public System.DateTime? RentFrom { get; set; }
        public System.DateTime? RentTo { get; set; }
        public decimal? Total { get; set; }
        public decimal PricePerDay { get; set; }
        public string RentNumber { get; set; }

        public string RegistrationNumber { get; set; }
        public string ModelName { get; set; }
        public string ManufacturerName { get; set; }
        public int YearOfProduction { get; set; }

        public string Email { get; set; }
        public string Name { get; set; }
        public string IdentityCard { get; set; }
        public string DrivingLicense { get; set; }
        public string ClientTelephone { get; set; }

        public string OfficeName { get; set; }
        public string OfficeStreet { get; set; }
        public string OfficeTelephone { get; set; }
        public string LocationName { get; set; }
        public string Currency { get; set; }
        public decimal Tax { get; set; }
        public decimal? TotalNetAmount { get; set; }
    }
}