﻿using System;
using AutoMapper;
using CarStation.Domain.Models;

namespace CarStation.Presentation.Models
{
    public class Manufacturer
    {
        public Guid _id;
        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Logo
        /// </summary>
        public string Logo { get; set; }
    }

    public class ManufacturerMapper
    {
        public IMapper EntityMapper;

        public ManufacturerMapper()
        {
            var entityConfig = new MapperConfiguration(cfg => cfg.CreateMap<ManufacturerDomainModel, Manufacturer>());

            EntityMapper = entityConfig.CreateMapper();
        }

    }
}