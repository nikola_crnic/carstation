﻿using System;
using AutoMapper;
using CarStation.Domain.Models;

namespace CarStation.Presentation.Models
{
    /// <summary>
    /// Location domain model
    /// </summary>
    public class Location
    {
        /// <summary>
        /// _id
        /// </summary>
        public Guid _id { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Country id
        /// </summary>
        public Guid CountryId { get; set; } 
        public Guid CurrencyId { get; set; } 

        public string Country { get; set; } 
        public string Currency { get; set; } 
        public decimal Tax { get; set; } 
    }

    public class LocationMapper
    {
        public IMapper EntityMapper;

        public LocationMapper()
        {
            var entityConfig = new MapperConfiguration(cfg => cfg.CreateMap<LocationDomainModel, Location>());
                ;
            EntityMapper = entityConfig.CreateMapper();
        }

    }
}