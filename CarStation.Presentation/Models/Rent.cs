﻿using System;
using AutoMapper;
using CarStation.Domain.Models;
using CarStation.Domain.Models.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace CarStation.Presentation.Models
{
    /// <summary>
    /// Rent domain model
    /// </summary>
    public class Rent
    {

        public Guid _id { get; set; }

        /// <summary>
        /// Note
        /// </summary>
        public string Note { get; set; }

        /// <summary>
        /// Rent number
        /// </summary>
        public string RentNumber { get; set; }

        /// <summary>
        /// Total net amount
        /// </summary>
        public decimal? TotalNetAmount { get; set; }

        /// <summary>
        /// Total gross amount
        /// </summary>
        public decimal? TotalGrossAmount { get; set; }

        /// <summary>
        /// Rented from date
        /// </summary>
        public DateTime RentedFrom { get; set; }

        /// <summary>
        /// Rented to date
        /// </summary>
        public DateTime RentedTo { get; set; }

        /// <summary>
        /// Client id
        /// </summary>
        public Guid? ClientId { get; set; }

        /// <summary>
        /// Office id
        /// </summary>
        public Guid OfficeId { get; set; }

        /// <summary>
        /// Vehicle id
        /// </summary>
        public Guid? VehicleId { get; set; }

        public decimal? Tax { get; set; }

        public string Clientname { get; set; }
        public string BranchOffice { get; set; }
        public string VehicleName { get; set; }
        public string Currency { get; set; }

        /// <summary>
        /// Rent status
        /// </summary>
        [JsonConverter(typeof(StringEnumConverter))]
        public RentStatus Status { get; set; } = RentStatus.Draft;
    }

    public class RentMapper
    {
        public IMapper EntityMapper;

        public RentMapper()
        {
            var entityConfig = new MapperConfiguration(cfg => cfg.CreateMap<RentDomainModel, Rent>());

            EntityMapper = entityConfig.CreateMapper();
        }

    }
}