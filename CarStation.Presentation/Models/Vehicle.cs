﻿using System;
using AutoMapper;
using CarStation.Domain.Models;

namespace CarStation.Presentation.Models
{
    /// <summary>
    /// Vehicle domain model
    /// </summary>
    public class Vehicle
    {
        /// <summary>
        /// Id
        /// </summary>
        public Guid _id { get; set; }

        /// <summary>
        /// Registration number
        /// </summary>
        public string RegistrationNumber { get; set; }

        /// <summary>
        /// Color in hex value
        /// </summary>
        public string Color { get; set; }

        /// <summary>
        /// Price per day
        /// </summary>
        public decimal PricePerDay { get; set; }

        /// <summary>
        /// Year of production
        /// </summary>
        public int YearOfProduction { get; set; }

        /// <summary>
        /// Picture
        /// </summary>
        public string Picture { get; set; }

        /// <summary>
        /// Branch office id
        /// </summary>
        public Guid BranchOfficeId { get; set; }

        /// <summary>
        /// Vehicle model id
        /// </summary>
        public Guid VehicleModelId { get; set; }

        public string VehicleModel { get; set; }
        public string BranchOffice { get; set; }
        public string Manufacturer { get; set; }
        public string Currency { get; set; }
        public string Fuel { get; set; }
        public string Type { get; set; }
        public string Status { get; set; }
        public int Passengers { get; set; }
        public bool IsAutomatic { get; set; }
    }

    public class VehicleMapper
    {
        public IMapper EntityMapper;

        public VehicleMapper()
        {
            var entityConfig = new MapperConfiguration(cfg => cfg.CreateMap<VehicleDomainModel, Vehicle>());

            EntityMapper = entityConfig.CreateMapper();
        }

    }
}