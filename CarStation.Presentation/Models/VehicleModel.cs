﻿using System;
using AutoMapper;
using CarStation.Domain.Models;
using CarStation.Domain.Models.Enums;

namespace CarStation.Presentation.Models
{
    /// <summary>
    /// VehicleModel model
    /// </summary>
    public class VehicleModel
    {

        public Guid _id { get; set; }
        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Number of passengers
        /// </summary>
        public int Passengers { get; set; }

        /// <summary>
        /// Indicates if vehicle has automatic transmission
        /// </summary>
        public bool IsAutomatic { get; set; }

        /// <summary>
        /// Manufacturer id
        /// </summary>
        public Guid ManufacturerId { get; set; }
        public string Manufacturer { get; set; }

        /// <summary>
        /// Type of fuel
        /// </summary>
        public Fuel Fuel { get; set; } = Fuel.Diesel;

        /// <summary>
        /// Type of vehicle
        /// </summary>
        public VehicleType Type { get; set; } = VehicleType.Compact;

        /// <summary>
        /// Status
        /// </summary>
        public VehicleStatus Status { get; set; } = VehicleStatus.Available;
    }

    public class VehicleModelMapper
    {
        public IMapper EntityMapper;

        public VehicleModelMapper()
        {
            var entityConfig = new MapperConfiguration(cfg => cfg.CreateMap<VehicleModelDomainModel, VehicleModel>());

            EntityMapper = entityConfig.CreateMapper();
        }

    }
}