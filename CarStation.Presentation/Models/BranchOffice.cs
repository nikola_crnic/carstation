﻿using System;
using AutoMapper;
using CarStation.Domain.Models;

namespace CarStation.Presentation.Models
{
    /// <summary>
    /// Branch office domain model
    /// </summary>
    public class BranchOffice
    {
        /// <summary>
        /// _id
        /// </summary>
        public Guid _id { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Street
        /// </summary>
        public string Street { get; set; }

        /// <summary>
        /// Location id
        /// </summary>
        public Guid LocationId { get; set; }

        /// <summary>
        /// Location
        /// </summary>
        public string LocationName { get; set; }

        /// <summary>
        /// Telephone number
        /// </summary>
        public string Telephone { get; set; }
    }

    public class BranchOfficeMapper
    {
        public IMapper EntityMapper;

        public BranchOfficeMapper()
        {
            var entityConfig = new MapperConfiguration(cfg => cfg.CreateMap<BranchOfficeDomainModel, BranchOffice>());

            EntityMapper = entityConfig.CreateMapper();
        }

    }
}