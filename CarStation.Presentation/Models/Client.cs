﻿using System;
using AutoMapper;
using CarStation.Domain.Models;

namespace CarStation.Presentation.Models
{
    /// <summary>
    /// Client domain model
    /// </summary>
    public class Client
    {
        /// <summary>
        /// _id
        /// </summary>
        public Guid _id { get; set; }

        /// <summary>
        /// Email address
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Identity card.
        /// </summary>
        public string IdentityCard { get; set; }

        /// <summary>
        /// Driving licence
        /// </summary>
        public string DrivingLicence { get; set; }

        /// <summary>
        /// Telephone number
        /// </summary>
        public string Telephone { get; set; }
    }

    public class ClientMapper
    {
        public IMapper EntityMapper;

        public ClientMapper()
        {
            var entityConfig = new MapperConfiguration(cfg => cfg.CreateMap<ClientDomainModel, Client>());

            EntityMapper = entityConfig.CreateMapper();
        }

    }
}