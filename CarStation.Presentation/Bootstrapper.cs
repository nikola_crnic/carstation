using System.Web.Mvc;
using CarStation.Database.Repositories;
using CarStation.Database.Repositories.Default;
using CarStation.Domain.Services;
using CarStation.Domain.Services.Default;
using Microsoft.Practices.Unity;
using Unity.Mvc3;

namespace CarStation.Presentation
{
    public static class Bootstrapper
    {
        public static void Initialise()
        {
            var container = BuildUnityContainer();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }

        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();     
            container.RegisterType<IRentService, RentService>();
            container.RegisterType<ILocationService, LocationService>();
            container.RegisterType<IBranchOfficeService, BranchOfficeService>();
            container.RegisterType<IClientService, ClientService>();
            container.RegisterType<IVehicleService, VehicleService>();
            container.RegisterType<IVehicleModelService, VehicleModelService>();
            container.RegisterType<IUserService, UserService>();
            container.RegisterType<IManufacturerService, ManufacturerService>();
            container.RegisterType<ICountryService, CountryService>();
            container.RegisterType<ICurrencyService, CurrencyService>();

            container.RegisterType<IRentRepository, RentRepository>();
            container.RegisterType<ILocationRepository, LocationRepository>();
            container.RegisterType<IBranchOfficeRepository, BranchOfficeRepository>();
            container.RegisterType<IClientRepository, ClientRepository>();
            container.RegisterType<IVehicleRepository, VehicleRepository>();
            container.RegisterType<IVehicleModelRepository, VehicleModelRepository>();
            container.RegisterType<IUserRepository, UserRepository>();
            container.RegisterType<IManufacturerRepository, ManufacturerRepository>();
            container.RegisterType<ICountryRepository, CountryRepository>();
            container.RegisterType<ICurrencyRepository, CurrencyRepository>();

            return container;
        }
    }
}