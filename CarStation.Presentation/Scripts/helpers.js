﻿function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
      s4() + '-' + s4() + s4() + s4();
}

function SerijskiBroj() {
    var dozvoljeniKarakteri = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZ";
    var duzina = 7;
    var serijskiBroj = '';
    for (var i = 0; i < duzina; i++) {
        var rnum = Math.floor(Math.random() * dozvoljeniKarakteri.length);
        serijskiBroj += dozvoljeniKarakteri.substring(rnum, rnum + 1);
    }

    return serijskiBroj;
}

function PorukeValidacija() {
    jQuery.extend(jQuery.validator.messages, {
        required: "Polje je obavezno.",
        remote: "Please fix this field.",
        email: "Unesite ispravnu email adresu.",
        url: "Unesite ispravan URL.",
        date: "Unesite ispravan datum.",
        dateISO: "Please enter a valid date (ISO).",
        number: "Unesite ispravan broj.",
        digits: "Samo cifre su dozvoljene.",
        creditcard: "Please enter a valid credit card number.",
        equalTo: "Please enter the same value again.",
        accept: "Please enter a value with a valid extension.",
        maxlength: jQuery.validator.format("Ne smijete unijeti više od {0} karaktera."),
        minlength: jQuery.validator.format("Morate unijeti najmanje {0} karaktera."),
        rangelength: jQuery.validator.format("Unos mora biti dužine između {0} i {1} karakter."),
        range: jQuery.validator.format("Please enter a value between {0} and {1}."),
        max: jQuery.validator.format("Unesite vrijednost manju ili jednaku {0}."),
        min: jQuery.validator.format("Unesite vrijednost veću ili jednaku {0}.")
    });
}

function convertCsharpToJsDate(csharpDate) {
    var dateArray = csharpDate.split('-');
    var day = dateArray[0];
    var month = dateArray[1];
    var year = dateArray[2];

    var dateString = year + "/" + month + "/" + day;

    return new Date(dateString);
};

