﻿using System.Configuration;

namespace CarStation.Database
{
    /// <summary>
    /// AppSettings properties
    /// </summary>
    public static class ConfigurationProperties
    {
        /// <summary>
        /// Database name
        /// </summary>
        public static string DbName { get; } = ConfigurationManager.AppSettings["DbName"];
        
        /// <summary>
        /// Database path
        /// </summary>
        public static string DbPath { get; } = ConfigurationManager.AppSettings["DbPath"];
    }
}