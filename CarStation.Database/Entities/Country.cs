﻿namespace CarStation.Database.Entities
{
    /// <summary>
    /// Country model
    /// </summary>
    public class Country : Entity
    {
        /// <summary>
        /// Name
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// Code
        /// </summary>
        public string code { get; set; }
    }
}