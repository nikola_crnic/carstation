﻿using System.ComponentModel.DataAnnotations;
using CarStation.Database.Enums;

namespace CarStation.Database.Entities
{
    /// <summary>
    /// User model
    /// </summary>
    public class User : Entity
    {
        /// <summary>
        /// Email
        /// </summary>
        [EmailAddress]
        [Required]
        public string Email { get; set; }

        /// <summary>
        /// Password
        /// </summary>
        [Required]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,15}$")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        /// <summary>
        /// Random data that hashes password
        /// </summary>
        [Required]
        public string PasswordSalt { get; set; }

        /// <summary>
        /// User role
        /// </summary>
        public UserRole Role { get; set; } = UserRole.User;
    }
}