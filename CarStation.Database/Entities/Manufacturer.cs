﻿using System.ComponentModel.DataAnnotations;

namespace CarStation.Database.Entities
{
    /// <summary>
    /// Manufacturer model
    /// </summary>
    public class Manufacturer : Entity
    {
        /// <summary>
        /// Name
        /// </summary>
        [Required]
        [MinLength(2)]
        [MaxLength(50)]
        public string Name { get; set; }

        /// <summary>
        /// Logo
        /// </summary>
        public string Logo { get; set; }
    }
}