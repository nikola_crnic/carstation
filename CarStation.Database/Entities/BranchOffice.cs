﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CarStation.Database.Entities
{
    /// <summary>
    /// Branch office model
    /// </summary>
    public class BranchOffice : Entity
    {
        /// <summary>
        /// Name
        /// </summary>
        [Required]
        [MinLength(2)]
        [MaxLength(50)]
        public string Name { get; set; }

        /// <summary>
        /// Street
        /// </summary>
        [Required]
        [MinLength(2)]
        [MaxLength(100)]
        public string Street { get; set; }

        /// <summary>
        /// Location id
        /// </summary>
        [Required]
        public Guid LocationId { get; set; }

        /// <summary>
        /// Telephone number
        /// </summary>
        [Required]
        [RegularExpression("(^[1-9][0-9]*)")]
        [MinLength(6)]
        [MaxLength(20)]
        public string Telephone { get; set; }
    }
}