﻿using System;
using System.ComponentModel.DataAnnotations;
using CarStation.Database.Enums;

namespace CarStation.Database.Entities
{
    /// <summary>
    /// VehicleModel model
    /// </summary>
    public class VehicleModel : Entity
    {
        /// <summary>
        /// Name
        /// </summary>
        [Required]
        [MinLength(2)]
        [MaxLength(50)]
        public string Name { get; set; }

        /// <summary>
        /// Number of passengers
        /// </summary>
        [Required]
        public int Passengers { get; set; }

        /// <summary>
        /// Indicates if vehicle has automatic transmission
        /// </summary>
        public bool IsAutomatic { get; set; }

        /// <summary>
        /// Manufacturer id
        /// </summary>
        [Required]
        public Guid ManufacturerId { get; set; }

        /// <summary>
        /// Type of fuel
        /// </summary>
        public Fuel Fuel { get; set; } = Fuel.Diesel;

        /// <summary>
        /// Type of vehicle
        /// </summary>
        public VehicleType Type { get; set; } = VehicleType.Compact;

        /// <summary>
        /// Status
        /// </summary>
        public VehicleStatus Status { get; set; } = VehicleStatus.Available;
    }
}