﻿using System;
using System.ComponentModel.DataAnnotations;
using CarStation.Database.Enums;
using CarStation.Database.Repositories;

namespace CarStation.Database.Entities
{
    /// <summary>
    /// Rent model
    /// </summary>
    public class Rent : Entity
    {
        /// <summary>
        /// Note
        /// </summary>
        public string Note { get; set; }

        /// <summary>
        /// Rent number
        /// </summary>
        public string RentNumber { get; set; }

        /// <summary>
        /// Total net amount
        /// </summary>
        public decimal? TotalNetAmount { get; set; }

        /// <summary>
        /// Total gross amount
        /// </summary>
        public decimal? TotalGrossAmount { get; set; }

        /// <summary>
        /// Rented from date
        /// </summary>
        [Required]
        public DateTime RentedFrom { get; set; }

        /// <summary>
        /// Rented to date
        /// </summary>
        [Required]
        public DateTime RentedTo { get; set; }

        /// <summary>
        /// Client id
        /// </summary>
        public Guid? ClientId { get; set; }

        /// <summary>
        /// Office id
        /// </summary>
        [Required]
        public Guid OfficeId { get; set; }

        /// <summary>
        /// Vehicle id
        /// </summary>
        public Guid? VehicleId { get; set; }

        public decimal Tax { get; set; }

        /// <summary>
        /// Rent status
        /// </summary>
        public RentStatus Status { get; set; } = RentStatus.Draft;
    }
}