﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CarStation.Database.Entities
{
    /// <summary>
    /// Location model
    /// </summary>
    public class Location : Entity
    {
        /// <summary>
        /// Name
        /// </summary>
        [Required]
        [MinLength(2)]
        [MaxLength(50)]
        public string Name { get; set; }

        /// <summary>
        /// Country
        /// </summary>
        [Required]
        public Guid CountryId { get; set; }

        /// <summary>
        /// Country
        /// </summary>
        [Required]
        public Guid CurrencyId { get; set; }

        /// <summary>
        /// Tax id
        /// </summary>
        [Required]
        public decimal Tax { get; set; }
    }
}