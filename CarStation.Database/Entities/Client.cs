﻿using System.ComponentModel.DataAnnotations;

namespace CarStation.Database.Entities
{
    /// <summary>
    /// Client model
    /// </summary>
    public class Client : Entity
    {
        /// <summary>
        /// Email address
        /// </summary>
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        [Required]
        [MinLength(2)]
        [MaxLength(50)]
        public string Name { get; set; }

        /// <summary>
        /// Identity card.
        /// </summary>
        [Required]
        [MinLength(2)]
        [MaxLength(20)]
        public string IdentityCard { get; set; }

        /// <summary>
        /// Driving licence
        /// </summary>
        [Required]
        [MinLength(2)]
        [MaxLength(20)]
        public string DrivingLicence { get; set; }

        /// <summary>
        /// Telephone number
        /// </summary>
        [RegularExpression("(^[1-9][0-9]*)")]
        [MinLength(6)]
        [MaxLength(20)]
        public string Telephone { get; set; }
    }
}