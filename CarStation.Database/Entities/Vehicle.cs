﻿using System;
using System.ComponentModel.DataAnnotations;
using CarStation.Database.Enums;

namespace CarStation.Database.Entities
{
    /// <summary>
    /// Vehicle model
    /// </summary>
    public class Vehicle : Entity
    {
        /// <summary>
        /// Registration number
        /// </summary>
        [Required]
        [MinLength(5)]
        [MaxLength(20)]
        public string RegistrationNumber { get; set; }

        /// <summary>
        /// Color in hex value
        /// </summary>
        [Required]
        public string Color { get; set; }

        /// <summary>
        /// Price per day
        /// </summary>
        [Required]
        public decimal PricePerDay { get; set; }

        /// <summary>
        /// Year of production
        /// </summary>
        [Required]
        public int YearOfProduction { get; set; }

        /// <summary>
        /// Picture
        /// </summary>
        public string Picture { get; set; }

        /// <summary>
        /// Branch office id
        /// </summary>
        [Required]
        public Guid BranchOfficeId { get; set; }

        /// <summary>
        /// Vehicle model id
        /// </summary>
        [Required]
        public Guid VehicleModelId { get; set; }
    }
}