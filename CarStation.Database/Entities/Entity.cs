﻿using System;
using MongoDB.Bson.Serialization.Attributes;

namespace CarStation.Database.Entities
{
    /// <summary>
    /// Entity model
    /// </summary>
    public class Entity
    {
        /// <summary>
        /// MongoDB id field.
        /// </summary>
        [BsonId]
        public Guid _id { get; set; } = Guid.NewGuid();

        /// <summary>
        /// Shows time of the latest entity change.
        /// </summary>
        public DateTime TimeStamp { get; private set; } = DateTime.Now;
    }
}