﻿using CarStation.Database.Entities;

namespace CarStation.Database.Repositories
{
    public interface IClientRepository : IRepository<Client>
    {
        void Update(Client client);
    }
}