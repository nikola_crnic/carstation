﻿using System;
using System.Collections.Generic;
using System.Linq;
using CarStation.Database.Entities;
using MongoDB.Bson;
using MongoDB.Driver;
using NLog;

namespace CarStation.Database.Repositories.Default
{
    /// <summary>
    /// Base database repository
    /// </summary>
    /// <typeparam name="TEntity">Entity type</typeparam>
    public abstract class Repository<TEntity> : IRepository<TEntity> where TEntity : Entity
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly IMongoDatabase _db;

        /// <summary>
        /// Repository constructor
        /// </summary>
        protected Repository()
        {
            IMongoClient client = new MongoClient(ConfigurationProperties.DbPath);
            _db = client.GetDatabase(ConfigurationProperties.DbName);
            MongoDefaults.GuidRepresentation = GuidRepresentation.CSharpLegacy;
        }

        /// <summary>
        /// Helper method that gets collection as queryable.
        /// </summary>
        /// <returns>Queryable collection</returns>
        public IQueryable<TEntity> GetCollectionAsQueryable()
        {
            return _db.GetCollection<TEntity>(typeof (TEntity).Name)
                .AsQueryable();
        }

        /// <summary>
        /// Helper method that gets mongo collection.
        /// </summary>
        /// <returns>Mongo collection</returns>
        public IMongoCollection<TEntity> GetCollection()
        {
            return _db.GetCollection<TEntity>(typeof (TEntity).Name);
        }

        /// <summary>
        /// Helper method that gets collection as BsonDocument.
        /// </summary>
        /// <returns>Queryable collection</returns>
        public IMongoCollection<BsonDocument> GetCollectionAsBsonDocument()
        {
            return _db.GetCollection<BsonDocument>(typeof(TEntity).Name);
        }

        /// <summary>
        /// Gets filter definition.
        /// </summary>
        /// <param name="field">Field name</param>
        /// <param name="value">Field value</param>
        /// <returns>Filter definition</returns>
        public FilterDefinition<BsonDocument> GetFilterDefinition<T>(string field, T value) where T : new ()
        {
            return Builders<BsonDocument>.Filter.Eq(field, value);
        }

        /// <summary>
        /// Gets list of T object.
        /// </summary>
        /// <returns>List of T objects.</returns>
        public IQueryable<TEntity> GetAll()
        {
            return
                GetCollectionAsQueryable();
        }

        /// <summary>
        /// Gets T object by id.
        /// </summary>
        /// <param name="id">Entity id.</param>
        /// <returns>T object.</returns>
        public TEntity GetById(Guid id)
        {
            return GetCollectionAsQueryable().FirstOrDefault(i => i._id == id);
        }

        /// <summary>
        /// Saves entity into db.
        /// </summary>
        /// <param name="data">T object.</param>
        public void Save(TEntity data)
        {
            var collection = GetCollection();
            try
            {
                collection.InsertOne(data);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
            }
        }

        /// <summary>
        /// Deletes entity by its id.
        /// </summary>
        /// <param name="id">Entity id</param>
        public void Delete(Guid id)
        {
            var collection = GetCollectionAsBsonDocument();
            var filter = GetFilterDefinition("_id", id);
            try
            {
                collection.DeleteOne(filter);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
            }
        }
    }
}