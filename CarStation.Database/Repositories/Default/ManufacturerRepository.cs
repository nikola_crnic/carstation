﻿using System;
using System.Linq;
using CarStation.Database.Entities;
using MongoDB.Bson;
using MongoDB.Driver;

namespace CarStation.Database.Repositories.Default
{
    /// <summary>
    /// Location database repository
    /// </summary>
    public class ManufacturerRepository : Repository<Manufacturer>, IManufacturerRepository
    {
        /// <summary>
        /// Updates manufacturer
        /// </summary>
        /// <param name="manufacturer">Manufacturer object</param>
        public void Update(Manufacturer manufacturer)
        {
            var filter = GetFilterDefinition("_id", manufacturer._id);
            var update = Builders<BsonDocument>.Update
                .Set("Name", manufacturer.Name)
                .CurrentDate("TimeStamp");
            var collection = GetCollectionAsBsonDocument();
            collection.UpdateOne(filter, update);
        }
    }
}