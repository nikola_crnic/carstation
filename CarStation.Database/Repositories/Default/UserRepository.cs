﻿using System;
using System.Linq;
using CarStation.Database.Entities;

namespace CarStation.Database.Repositories.Default
{
    /// <summary>
    /// User database repository
    /// </summary>
    public class UserRepository : Repository<User>, IUserRepository
    {
    }
}