﻿using System;
using System.Linq;
using CarStation.Database.Entities;
using MongoDB.Bson;
using MongoDB.Driver;

namespace CarStation.Database.Repositories.Default
{
    /// <summary>
    /// Client database repository
    /// </summary>
    public class ClientRepository : Repository<Client>, IClientRepository
    {
        /// <summary>
        /// Updates client
        /// </summary>
        /// <param name="client">Client object</param>
        public void Update(Client client)
        {
            var filter = GetFilterDefinition("_id", client._id);
            var update = Builders<BsonDocument>.Update
                .Set("Name", client.Name)
                .Set("DrivingLicence", client.DrivingLicence)
                .Set("Telephone", client.Telephone)
                .Set("IdentityCard", client.IdentityCard)
                .CurrentDate("TimeStamp");
            var collection = GetCollectionAsBsonDocument();
            collection.UpdateOne(filter, update);
        }
    }
}