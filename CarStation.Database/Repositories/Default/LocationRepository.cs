﻿using System;
using System.Linq;
using CarStation.Database.Entities;
using MongoDB.Bson;
using MongoDB.Driver;

namespace CarStation.Database.Repositories.Default
{
    /// <summary>
    /// Location database repository
    /// </summary>
    public class LocationRepository : Repository<Location>, ILocationRepository
    {
        /// <summary>
        /// Updates location
        /// </summary>
        /// <param name="location">Location object</param>
        public void Update(Location location)
        {
            var filter = GetFilterDefinition("_id", location._id);
            var update = Builders<BsonDocument>.Update
                .Set("Name", location.Name)
                .Set("CountryId", location.CountryId)
                .CurrentDate("TimeStamp");
            var collection = GetCollectionAsBsonDocument();
            collection.UpdateOne(filter, update);
        }
    }
}