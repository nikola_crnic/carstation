﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using CarStation.Database.Entities;
using CarStation.Database.Enums;
using MongoDB.Bson;
using MongoDB.Driver;

namespace CarStation.Database.Repositories.Default
{
    /// <summary>
    /// Rent database repository
    /// </summary>
    public class RentRepository : Repository<Rent>, IRentRepository
    {
        /// <summary>
        /// Updates rent status
        /// </summary>
        public void Update(Rent rent)
        {
            var filter = GetFilterDefinition("_id", rent._id);
            var update = Builders<BsonDocument>.Update
                .Set("Status", rent.Status)
                .Set("ClientId", rent.ClientId)
                .Set("VehicleId", rent.VehicleId)
                .Set("OfficeId", rent.OfficeId)
                .Set("RentedFrom", rent.RentedFrom)
                .Set("RentedTo", rent.RentedTo)
                .Set("TotalNetAmount", rent.TotalNetAmount)
                .Set("TotalGrossAmount", rent.TotalGrossAmount)
                .CurrentDate("TimeStamp");
            var collection = GetCollectionAsBsonDocument();
            collection.UpdateOne(filter, update);
        }
    }
}