﻿using System;
using System.IO;
using System.Linq;
using System.Web;
using CarStation.Database.Entities;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using NLog;

namespace CarStation.Database.Repositories.Default
{
    /// <summary>
    /// Country database repository
    /// </summary>
    public class CountryRepository : Repository<Country>, ICountryRepository
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Saves countries from countries.json
        /// </summary>
        public void Save()
        {
            var filePath = HttpContext.Current.Server.MapPath("App_Data/Commons/countries.json");
            var text = System.IO.File.ReadAllText(filePath);
            var documents = BsonSerializer.Deserialize<BsonArray>(text).Select(p => p.AsBsonDocument);
            var collection = GetCollectionAsBsonDocument();
            try
            {
                foreach (var document in documents)
                {
                    var country = BsonSerializer.Deserialize<Country>(document);
                    var serializedDocument = country.ToBsonDocument();
                    collection.InsertOne(serializedDocument);
                }

            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
            }
        }
    }
}