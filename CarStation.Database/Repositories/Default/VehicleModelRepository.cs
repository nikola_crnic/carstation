﻿using System;
using System.Linq;
using CarStation.Database.Entities;
using CarStation.Database.Enums;
using MongoDB.Bson;
using MongoDB.Driver;

namespace CarStation.Database.Repositories.Default
{
    /// <summary>
    /// Vehicle model database repository
    /// </summary>
    public class VehicleModelRepository : Repository<VehicleModel>, IVehicleModelRepository
    {
        
        /// <summary>
        /// Updates vehicle model
        /// </summary>
        /// <param name="vehicleModel">Vehicle model object</param>
        public void Update(VehicleModel vehicleModel)
        {
            var filter = GetFilterDefinition("_id", vehicleModel._id);
            var update = Builders<BsonDocument>.Update
                .Set("Name", vehicleModel.Name)
                .Set("Passengers", vehicleModel.Passengers)
                .Set("IsAutomatic", vehicleModel.IsAutomatic)
                .Set("Fuel", vehicleModel.Fuel)
                .Set("Status", vehicleModel.Status)
                .Set("Type", vehicleModel.Type)
                .CurrentDate("TimeStamp");
            var collection = GetCollectionAsBsonDocument();
            collection.UpdateOne(filter, update);
        }
    }
}