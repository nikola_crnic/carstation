﻿using System;
using System.Collections.Generic;
using System.Linq;
using CarStation.Database.Entities;
using MongoDB.Bson;
using MongoDB.Driver;

namespace CarStation.Database.Repositories.Default
{
    public class BranchOfficeRepository : Repository<BranchOffice>, IBranchOfficeRepository
    {
        /// <summary>
        /// Updates branch office
        /// </summary>
        /// <param name="branchOffice">Branch office object</param>
        public void Update(BranchOffice branchOffice)
        {
            var filter = GetFilterDefinition("_id", branchOffice._id);
            var update = Builders<BsonDocument>.Update
                .Set("Name", branchOffice.Name)
                .Set("Street", branchOffice.Street)
                .Set("LocationId", branchOffice.LocationId)
                .Set("Telephone", branchOffice.Telephone)
                .CurrentDate("TimeStamp");
            var collection = GetCollectionAsBsonDocument();
            collection.UpdateOne(filter, update);
        }
    }
}