﻿using System;
using System.Linq;
using CarStation.Database.Entities;
using MongoDB.Bson;
using MongoDB.Driver;

namespace CarStation.Database.Repositories.Default
{
    /// <summary>
    /// Vehicle database repository
    /// </summary>
    public class VehicleRepository : Repository<Vehicle>, IVehicleRepository
    {

        /// <summary>
        /// Updates vehicle
        /// </summary>
        /// <param name="vehicle">Vehicle object</param>
        public void Update(Vehicle vehicle)
        {
            var filter = GetFilterDefinition("_id", vehicle._id);
            var update = Builders<BsonDocument>.Update
                .Set("RegistrationNumber", vehicle.RegistrationNumber)
                .Set("Color", vehicle.Color)
                .Set("PricePerDay", vehicle.PricePerDay)
                .Set("YearOfProduction", vehicle.YearOfProduction)
                .Set("Picture", vehicle.Picture)
                .Set("BranchOfficeId", vehicle.BranchOfficeId)
                .Set("VehicleModelId", vehicle.VehicleModelId)
                .CurrentDate("TimeStamp");
            var collection = GetCollectionAsBsonDocument();
            collection.UpdateOne(filter, update);
        }
    }
}