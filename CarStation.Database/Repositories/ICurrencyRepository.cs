using CarStation.Database.Entities;

namespace CarStation.Database.Repositories
{
    public interface ICurrencyRepository : IRepository<Currency>
    {
        void Save();
    }
}