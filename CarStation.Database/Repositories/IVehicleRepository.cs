﻿using CarStation.Database.Entities;

namespace CarStation.Database.Repositories
{
    /// <summary>
    /// Vehicle repository interface
    /// </summary>
    public interface IVehicleRepository : IRepository<Vehicle>
    {
        void Update(Vehicle vehicle);
    }
}