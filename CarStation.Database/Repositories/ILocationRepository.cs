﻿using CarStation.Database.Entities;

namespace CarStation.Database.Repositories
{
    public interface ILocationRepository : IRepository<Location>
    {
        void Update(Location location);
    }
}