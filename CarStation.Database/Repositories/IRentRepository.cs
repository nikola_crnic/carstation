﻿using System;
using System.Collections.Generic;
using CarStation.Database.Entities;
using CarStation.Database.Enums;

namespace CarStation.Database.Repositories
{
    /// <summary>
    /// Rent repository interface
    /// </summary>
    public interface IRentRepository : IRepository<Rent>
    {
        void Update(Rent rent);
    }
}