using CarStation.Database.Entities;

namespace CarStation.Database.Repositories
{
    public interface IManufacturerRepository : IRepository<Manufacturer>
    {
        void Update(Manufacturer manufacturer);
    }
}