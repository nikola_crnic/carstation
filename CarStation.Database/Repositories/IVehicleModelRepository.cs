using CarStation.Database.Entities;
using CarStation.Database.Enums;

namespace CarStation.Database.Repositories
{
    public interface IVehicleModelRepository : IRepository<VehicleModel>
    {
        void Update(VehicleModel vehicleModel);
    }
}