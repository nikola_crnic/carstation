﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CarStation.Database.Repositories
{
    public interface IRepository<T>
    {
        IQueryable<T> GetAll();
        T GetById(Guid id);
        void Save(T data);
        void Delete(Guid id);
    }
}