﻿using CarStation.Database.Entities;

namespace CarStation.Database.Repositories
{
    public interface ICountryRepository : IRepository<Country>
    {
        void Save();
    }
}