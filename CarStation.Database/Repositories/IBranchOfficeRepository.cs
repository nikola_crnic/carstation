﻿using System;
using System.Collections.Generic;
using System.Linq;
using CarStation.Database.Entities;

namespace CarStation.Database.Repositories
{
    /// <summary>
    /// Branch office repository interface
    /// </summary>
    public interface IBranchOfficeRepository : IRepository<BranchOffice>
    {
        void Update(BranchOffice branchOffice);
    }
}