﻿namespace CarStation.Database.Enums
{
    public enum UserRole
    {
         Admin = 1,
         User = 2
    }
}