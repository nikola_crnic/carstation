﻿namespace CarStation.Database.Enums
{
    /// <summary>
    /// Rent status
    /// </summary>
    public enum RentStatus
    {
         Draft = 1,
         New = 2,
         Paid = 3,
         NotPaid = 4,
         Overdue = 5
    }
}